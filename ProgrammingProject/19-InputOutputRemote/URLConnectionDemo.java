import java.net.URL;
import java.net.URLConnection;
import java.net.HttpURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class URLConnectionDemo {
	public static void main(String[] args) {
		try {
			URL url = new URL("http://www.inf.unibz.it/~tkalcic/misc/action.php?action=kill&target=professor");
			URLConnection uc = url.openConnection();

			if (! (uc instanceof HttpURLConnection))
				throw new RuntimeException("Not an HTTP URL");

			BufferedReader in = new BufferedReader(new InputStreamReader(((HttpURLConnection)uc).getInputStream()));
			String buffer = null;

			while ((buffer = in.readLine()) != null)
				System.out.println(buffer);

		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
}
