import java.net.Socket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import java.net.URL;
import java.net.URLConnection;
import java.net.HttpURLConnection;

public class SocketVsURLConn {
	public static void socket() throws IOException {
		Socket client = new Socket("www.rai.it", 80);
		DataOutputStream out = new DataOutputStream(client.getOutputStream());
		out.write("GET /index.php HTTP/1.0\r\n\r\n".getBytes());
		out.flush();

		BufferedReader in = new BufferedReader(new InputStreamReader(new DataInputStream(client.getInputStream())));
		String buffer = null;

		while ((buffer = in.readLine()) != null)
			System.out.println(buffer);

	}

	public static void URLConnection() throws IOException {
		URLConnection uc = new URL("http://www.rai.it").openConnection();
		if (! (uc instanceof HttpURLConnection))
			throw new RuntimeException("Not a valid HTTP address");

		BufferedReader in = new BufferedReader(new InputStreamReader(((HttpURLConnection)uc).getInputStream()));
		String buffer = null;

		while ((buffer = in.readLine()) != null)
			System.out.println(buffer);
	}

	public static void main(String[] args) throws IOException {
		socket();
		URLConnection();
	}
}
