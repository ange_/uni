import java.net.Socket;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.DataInputStream;

public class HTTPRequestExample {
	public static void main(String[] args) throws IOException {
		Socket sock = new Socket("www.rai.it", 80);

		DataOutputStream out = new DataOutputStream(sock.getOutputStream());
		String getString = "GET /index.php HTTP/1.1\r\n\r\n";
		out.write(getString.getBytes());
		out.flush();

		BufferedReader reader = new BufferedReader(new InputStreamReader(new DataInputStream(sock.getInputStream())));
		String buffer = null;

		while ((buffer = reader.readLine()) != null)
			System.out.println(buffer);
	}
}
