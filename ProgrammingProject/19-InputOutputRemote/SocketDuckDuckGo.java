import java.net.Socket;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.DataOutputStream;

public class SocketDuckDuckGo {
	public static void main(String[] args) {
		try (Socket conn = new Socket("duckduckgo.com", 80)) {
			DataOutputStream out = new DataOutputStream(conn.getOutputStream());
			out.write("GET /?q=bob+dylan&t=hg&ia=web HTTP/1.0\r\n\r\n".getBytes());
			out.flush();

			BufferedReader in = new BufferedReader(new InputStreamReader(new DataInputStream(conn.getInputStream())));
			String buffer = null;

			while ((buffer = in.readLine()) != null)
				System.out.println(buffer);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
