import java.net.URL;
import java.net.URLConnection;
import java.net.HttpURLConnection;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class RESTExample {
	public static void main(String[] args) {
		String apikey = "someAPIKey";
		String urlAddress = "http://api.openweathermap.org/data/2.5/forecast/daily";
		String result = "";
		try {
			URLConnection url = new URL(urlAddress + "?APPID=" + apikey + "&q=bolzano").openConnection();

			if (! (url instanceof HttpURLConnection))
				throw new RuntimeException("Not an HTTP URL");

			BufferedReader in = new BufferedReader(new InputStreamReader(((HttpURLConnection)url).getInputStream()));
			String buffer = null;

			while ((buffer = in.readLine()) != null)
				result += buffer;

			
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println(result);
	}
}
