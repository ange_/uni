import java.net.Socket;
import java.net.ServerSocket;
import java.io.IOException;
import java.io.DataOutputStream;
import java.io.OutputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.net.SocketTimeoutException;

public class ServerExample extends Thread {
	private ServerSocket servsock;

	public ServerExample(int port) throws IOException {
		servsock = new ServerSocket(port);
		servsock.setSoTimeout(10000);
	}

	public void run() {
		for (;;) {
			try {
				System.out.println("Waiting on port: " + servsock.getLocalPort());
				Socket client_socket = servsock.accept();

				System.out.println("Connected to: " + client_socket.getRemoteSocketAddress());

				DataInputStream in = new DataInputStream(client_socket.getInputStream());
				System.out.println(in.readUTF());

				DataOutputStream out = new DataOutputStream(client_socket.getOutputStream());
				out.writeUTF("You have been connected.\nGoodbye.");
				
				client_socket.close();	
			} catch (SocketTimeoutException e) {
				System.err.println("Timed out.");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		if (args.length != 1)
			throw new RuntimeException("Usage: " + ServerExample.class.getName() + " [port]");

		final int port = Integer.parseInt(args[0]);

		try {
			Thread t = new ServerExample(port);
			t.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
