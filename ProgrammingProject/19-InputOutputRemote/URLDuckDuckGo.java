import java.net.URL;
import java.net.URLConnection;
import java.net.HttpURLConnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class URLDuckDuckGo {
	public static void main(String[] args) throws IOException {
		URLConnection uc = new URL("https://duckduckgo.com/?q=bob+dylan&t=hg&ia=web").openConnection();
		if (! (uc instanceof HttpURLConnection))
			throw new RuntimeException("Invalid HTTP URL");

		HttpURLConnection conn = (HttpURLConnection)uc;
		conn.setReadTimeout(5000);
		conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
		conn.addRequestProperty("User-Agent", "Mozilla");

		BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
		String buffer = null;

		while ((buffer = in.readLine()) != null)
			System.out.println(buffer);
	}
}
