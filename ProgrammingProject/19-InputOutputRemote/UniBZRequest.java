import java.net.Socket;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.DataInputStream;

public class UniBZRequest {
	public static void main(String[] args) throws IOException {
		Socket sock = new Socket("www.google.it", 80);

		DataOutputStream out = new DataOutputStream(sock.getOutputStream());
		String getString = "GET /search?q=unibz HTTP/1.1\r\n\r\n";
		out.write(getString.getBytes("ASCII"));
		out.flush();

		BufferedReader reader = new BufferedReader(new InputStreamReader(new DataInputStream(sock.getInputStream())));
		String buffer = null;

		while ((buffer = reader.readLine()) != null)
			System.out.println(buffer);
	}
}
