import java.net.Socket;
import java.io.IOException;
import java.io.DataOutputStream;
import java.io.OutputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.net.UnknownHostException;

public class ClientExample {
	public static void main(String[] args) {
		if (args.length != 2)
			throw new RuntimeException("Usage: " + ClientExample.class.getName() + " [server] [port]");

		final String server = args[0];
		final int port = Integer.parseInt(args[1]);

		try (Socket sock = new Socket(server, port)) {
			System.out.println("Connected to " + sock.getRemoteSocketAddress());
			DataOutputStream out = new DataOutputStream(sock.getOutputStream());

			out.writeUTF("Hello from: " + sock.getLocalAddress());

			DataInputStream in = new DataInputStream(sock.getInputStream());
			System.out.println("Server says: " + in.readUTF());
		} catch (UnknownHostException e) {
			System.err.println(server + " has not been found.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
