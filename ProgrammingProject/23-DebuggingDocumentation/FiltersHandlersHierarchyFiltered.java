import java.util.logging.Logger;
import java.util.logging.ConsoleHandler;
import java.util.logging.Filter;
import java.util.logging.LogRecord;

public class FiltersHandlersHierarchyFiltered {
	public static void main(String[] args) {
		Logger logger = Logger.getLogger("");

		Logger logger1 = Logger.getLogger("1");
		Logger logger2 = Logger.getLogger("1.2");

		logger1.addHandler(new ConsoleHandler());
		logger2.addHandler(new ConsoleHandler());

		logger1.setFilter(new Filter() {
			public boolean isLoggable(LogRecord record) {
				return false;
			}
		});

		logger.info("msg: ");
		logger1.info("msg1: ");
		logger2.info("msg1.2: ");
	}
}
