public class Step {
	public int currentProduct = 0;

	public static void main(String[] args) {
		Step st = new Step();
		String s = "Something in the way ..."; st.run(s);
		System.out.println(s);
	}

	public void run(String s) {
		int j = 7;
		int k = 0;
		for (int i= 0; i < 10; i++) {
			k = doSomething(i,j);
			this.currentProduct = k;
			System.out.println("The product is" + " " + k);
		}
	}

	private int doSomething(int i, int j) {
		int prod = i*j;
		return prod;
	}

	public String presentMyself() {
		String s = "This is the Step class.";
		return s;
	}
}
