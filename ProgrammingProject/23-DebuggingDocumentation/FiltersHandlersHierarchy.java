import java.util.logging.Logger;
import java.util.logging.ConsoleHandler;


public class FiltersHandlersHierarchy {
	public static void main(String[] args) {
		Logger logger = Logger.getLogger("");

		Logger logger1 = Logger.getLogger("1");
		Logger logger2 = Logger.getLogger("1.2");

		logger1.addHandler(new ConsoleHandler());
		logger2.addHandler(new ConsoleHandler());

		logger.info("msg: ");
		logger1.info("msg1: ");
		logger2.info("msg1.2: ");
	}
}
