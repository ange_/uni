import java.util.logging.Logger;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.io.IOException;

public class ProcessorHandled {
	public void doSomething() throws IOException {
		Logger logger = Logger.getLogger(this.getClass().getName());
		logger.addHandler(new FileHandler("out.log"));

		int sum = 0;
		for (int i = 0; i < 100; ++i) {
			sum += i;
			logger.log(Level.WARNING, logger.getName() + ": Sum is: " + sum);
		}

		// removing handler
		logger.removeHandler(logger.getHandlers()[0]);
	}
}
