public class Logging01 {
	public static void main(String[] args) {
		Logging01 obj = new Logging01();
		obj.run();
	}

	public void run() {
		final int a = 5;
		int res = 0;

		for (int i = 0; i < 100; ++i) {
			res = i*a;
			System.out.println("res at " + i + " --> " + res);

			if (res > 255)
				System.err.println("Too large");
		}
	}
}
