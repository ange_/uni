import java.util.logging.Logger;

public class SingleLogger {
	private static volatile Logger instance = null;

	public static Logger getInstance() {
		if (instance == null) {
			synchronized(SingleLogger.class) {
				if (instance == null)
					instance = Logger.getLogger(SingleLogger.class.getName());
			}
		}

		return instance;
	}
}
