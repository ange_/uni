import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.logging.ConsoleHandler;

public class LogLevelTest {
	public static void main(String[] args) {
		Logger logger = Logger.getLogger(LogLevelTest.class.getName());
		logger.setLevel(Level.FINE);

		logger.addHandler(new ConsoleHandler());

		logger.log(Level.FINEST, "b = 6");
		logger.log(Level.FINER, "a = 5");
		logger.log(Level.FINE, "i = 5");
		logger.log(Level.CONFIG, "numOfWorkers = 5");
		logger.log(Level.INFO, "DB Connection Established");
		logger.log(Level.WARNING, "String very long");
		logger.log(Level.SEVERE, "Fatal error");
	}
}
