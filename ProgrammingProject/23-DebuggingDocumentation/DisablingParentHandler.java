import java.util.logging.Logger;
import java.util.logging.ConsoleHandler;


public class DisablingParentHandler {
	public static void main(String[] args) {
		Logger logger = Logger.getLogger("1");

		Logger logger1 = Logger.getLogger("1.2");
		Logger logger2 = Logger.getLogger("1.2.3");

		logger1.addHandler(new ConsoleHandler());
		logger2.addHandler(new ConsoleHandler());

		logger.setUseParentHandlers(false);

		logger.info("msg1: ");
		logger1.info("msg1.2: ");
		logger2.info("msg1.2.3: ");
	}
}
