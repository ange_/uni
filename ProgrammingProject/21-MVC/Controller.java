public class Controller {
	Car c = null;

	public void runApp() {
		c = new Car();

		SpeedView sv = new SpeedView();
		RpmView rv = new RpmView();
		GearView gv = new GearView();
		CommandView cv = new CommandView();
		
		c.addView(sv);
		c.addView(rv);
		c.addView(gv);

		cv.addActionListener(this);

		while (true)
			cv.getInput();
	}

	public void commandHandler(String cmd) {
		System.out.println("commandHandler: "+cmd);
		if (cmd == null)
			return;

		if (cmd.equalsIgnoreCase("gas"))
			c.speedUp();
		if (cmd.equalsIgnoreCase("brake"))
			c.speedDown();
		if (cmd.equalsIgnoreCase("gearup"))
			c.gearUp();
		if (cmd.equalsIgnoreCase("geardown"))
			c.gearDown();

		c.notifyViews();
	}
}