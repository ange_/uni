public class Main {
	public static void main(String[] args) {
		ObservableValue ov = new ObservableValue(0);
		TextObserver to = new TextObserver(ov);

		ov.addObserver(to);
		ov.setValue(5);
	}
}