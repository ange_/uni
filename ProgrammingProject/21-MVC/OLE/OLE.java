import java.util.Observable;
import java.util.List;
import java.util.ArrayList;

public class OLE extends Observable {
	private List<String> messages = new ArrayList<>();

	public int size() {
		return messages.size();
	}

	public String get(int index) {
		return messages.get(index);
	}

	public void add(String message) {
		messages.add(message);
		this.setChanged();
		this.notifyObservers();
	}
}