public class Runner {
	public static void main(String[] args) {
		OLE ole = new OLE();
		Student jimmy = new Student("Jimmy");
		Student gina = new Student("Gina");
		ole.addObserver(jimmy);
		ole.addObserver(gina);
		ole.add("New results are online.");
		ole.add("Noone has passed the exam.");
	}
}