import java.util.Observer;
import java.util.Observable;

public class Student implements Observer {
	private String name = null;

	public Student(String name) {
		this.name = name;
	}

	@Override
	public void update(Observable obs, Object obj) {
		OLE o = (OLE)obs;
		System.out.printf("[ %s ] : %s\n", this.name, o.get(o.size() - 1));
	}
}