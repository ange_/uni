public class GearView extends View {
	GearView() {
		viewName = "Gear View";
	}

	@Override
	public void updateView(Model m) {
		int gear = ((Car)m).getGear();
		System.out.println("[ " + viewName + "] Gear = " + gear);
	}
}