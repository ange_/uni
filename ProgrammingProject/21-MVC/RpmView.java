public class RpmView extends View {
	RpmView() {
		viewName = "Rpm View";
	}

	@Override
	public void updateView(Model m) {
		int rpm = ((Car)m).getRPM();
		System.out.println("[ " + viewName + "] RPM = " + rpm);
	}
}