public class SpeedView extends View {
	SpeedView() {
		viewName = "Speed View";
	}

	public void updateModel(Model m) {
		int speed = ((Car)m).getSpeed();
		System.out.println("[ " + viewName + "] Speed = " + speed);
	}
}