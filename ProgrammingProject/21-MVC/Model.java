import java.util.List;
import java.util.ArrayList;

public class Model {
	private List<View> listOfViews = new ArrayList<>();

	public void notifyViews() {
		for (View w : listOfViews)
			w.updateView(this);
	}

	public void addView(View w) {
		listOfViews.add(w);
	}
}