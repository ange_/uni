import java.util.List;
import java.util.ArrayList;

public class View {
	public String viewName = "";
	private List<Controller> listOfControllers = new ArrayList<>();
	public String cmd = null;

	public void updateView(Model m) { }
	public void addActionListener(Controller c) {
		listOfControllers.add(c);
	}

	public void invokeControllers() {
		for (Controller c : listOfControllers)
			c.commandHandler(cmd);
	}
}