import java.io.IOException;

public class CommandView extends View {
	CommandView() {
		viewName = "Command View";
	}

	public void getInput() {
		System.out.println("Q/A --> Gas/Brake");
		System.out.println("W/S --> Gear up/down");

		try {
			cmd = null;

			int in = System.in.read();
			System.out.println(in);

			if (in ==113)
				cmd = "gas";
			if (in ==97)
				cmd = "brake";
			if (in ==119)
				cmd = "gearup";
			if (in ==115)
				cmd = "geardown";

			invokeControllers();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}