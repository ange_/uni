public class Car extends Model {
	public int speed = 0;
	public int RPM = 100;
	public int gear = 0;


	public int getSpeed() {
		return speed;
	}
	public int getRPM() {
		return RPM;
	}
	public int getGear() {
		return gear;
	}

	public void speedUp() {
		RPM += 100;
		updateSpeed();
	}

	public void speedDown() {
		RPM -= 100;
		updateSpeed();
	}

	public void gearUp() {
		++gear;
		RPM = 100;
		updateSpeed();
	}

	public void gearDown() {
		--gear;
		RPM = 1000;
		updateSpeed();
	}

	public void updateSpeed() {
		speed = RPM * gear/100;
	}
}