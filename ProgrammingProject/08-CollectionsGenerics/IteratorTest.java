import java.util.*;

public class IteratorTest {
	public static void main(String[] args) {
		ArrayList<String> strings = new ArrayList<>();
		strings.add("First");
		strings.add("Second");
		strings.add("Third");

		Iterator itr = strings.iterator();

		while (itr.hasNext())
			System.out.println(itr.next());
	}
}
