import java.util.*;

class Dog {
	public String name;
	public int age;

	public Dog(String name, int age) {
		this.name = name;
		this.age = age;
	}
}

public class DogIterator {
	public static void main(String[] args) {
		Dog d1 = new Dog("King", 5);
		Dog d2 = new Dog("Rex", 7);
		Dog d3 = new Dog("Boss", 2);
		Dog d4 = new Dog("Duke", 11);
		ArrayList<Dog> dogs = new ArrayList<Dog>();
		dogs.add(d1);
		dogs.add(d2);
		dogs.add(d3);
		dogs.add(d4);
		//get avg age
		Iterator<Dog> itr = dogs.iterator();
		int sumage = 0;
		while (itr.hasNext())
			sumage += itr.next().age;
		int avgage = sumage/4;

		itr = dogs.iterator();
		while (itr.hasNext()) {
			Dog temp = itr.next();
			if (temp.age >= avgage)
				System.out.println(temp.name);
		}
	}
}
