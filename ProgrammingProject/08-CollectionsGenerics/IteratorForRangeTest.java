import java.util.*;

public class IteratorForRangeTest {
	public static void main(String[] args) {
		ArrayList<String> strings = new ArrayList<>();
		strings.add("First");
		strings.add("Second");
		strings.add("Third");

		Iterator itr = strings.iterator();

		System.out.println("Using \"var\"");
		for (var string : strings)
			System.out.println(string);

		System.out.println("\nNormal in-range loop");
		for (String string : strings)
			System.out.println(string);

	}
}
