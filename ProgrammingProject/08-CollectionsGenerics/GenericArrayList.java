import java.util.ArrayList;

public class GenericArrayList {
	public static void main(String[] args) {
		ArrayList<String> strings = new ArrayList<>();
		strings.add("First");
		strings.add("Second");
		strings.add("Third");

		String first = strings.get(0), second = strings.get(1), third = strings.get(2);
		System.out.println(first + "\n" + second + "\n" + third);
	}
}
