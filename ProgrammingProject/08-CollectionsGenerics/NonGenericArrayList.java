import java.util.ArrayList;

public class NonGenericArrayList {
	public static void main(String[] args) {
		ArrayList strings = new ArrayList();
		strings.add("First");
		strings.add("Second");
		strings.add("Third");

		String first = (String)strings.get(0), second = (String)strings.get(1), third = (String)strings.get(2);
		System.out.println(first + "\n" + second + "\n" + third);
	}
}
