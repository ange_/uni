import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

class MyNode<String> {
	private String value = null;
	private MyNode<String> parent = null;
	private List<MyNode<String>> children = new ArrayList<>();

	MyNode(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void addChild(MyNode<String> c) {
		children.add(c);
	}

	public List<MyNode<String>> getChildren() {
		return children;
	}

	public void setParent(MyNode<String> parent) {
		this.parent = parent;
	}

	public MyNode<String> getParent() {
		return parent;
	}
}

public class MyTree {
	public static void main(String args[]) {
		MyNode<String> r = new MyNode<>("root");
		MyNode<String> n1 = new MyNode<>("node 1");
		MyNode<String> n2 = new MyNode<>("node 2");

		r.addChild(n1);
		r.addChild(n2);

		MyNode<String> n11 = new MyNode<>("node 11");
		MyNode<String> n12 = new MyNode<>("node 12");
		n1.addChild(n11);
		n1.addChild(n12);

		MyNode<String> n21 = new MyNode<>("node 21");
		MyNode<String> n22 = new MyNode<>("node 22");
		n2.addChild(n21);
		n2.addChild(n22);

		/* Searching every node containing "2" */
		List<MyNode<String>> rc = r.getChildren();
		Iterator<MyNode<String>> ir = rc.iterator();

		while (ir.hasNext()) {
			MyNode<String> child = ir.next();
			if (child.getValue().contains("2"))
				System.out.println(child.getValue());

			Iterator<MyNode<String>> ic = child.getChildren().iterator();

			while (ic.hasNext()) {
				MyNode<String> grandChild = ic.next();
				if (child.getValue().contains("2"))
					System.out.println("+---" + grandChild.getValue());
			}
		}
	}
}
