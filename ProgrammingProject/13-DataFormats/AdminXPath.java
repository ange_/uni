import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.FileInputStream;
import java.io.IOException;

public class AdminXPath {
	public static void main(String[] args) {
		DocumentBuilder parser = null;
		try {
			parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		Document xmlDoc = null;
		try {
			xmlDoc = parser.parse(new FileInputStream("./employees.xml"));
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}

		XPath xpath = XPathFactory.newInstance().newXPath();
		String expr = "/Employees/Employee[@type='admin']";

		NodeList adminEntries = null;
		try {
			adminEntries = (NodeList)xpath.compile(expr).evaluate(xmlDoc, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}

		for (int ni = 0; ni < adminEntries.getLength(); ++ni) {
			Node admin = adminEntries.item(ni);
			if (admin != null) {
				NodeList adminData = admin.getChildNodes();
				for (int i = 0; i < adminData.getLength(); ++i)
					if (adminData.item(i).getNodeType() == Node.ELEMENT_NODE)
						System.out.println(adminData.item(i).getNodeName() + ": " + adminData.item(i).getFirstChild().getNodeValue());
			}
		}
	}
}
