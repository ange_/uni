/*
 * NEEDS JSON-simple (https://code.google.com/p/json-simple/)
 * TO WORK
 */

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSONParsing {
	public static void main(String[] args) {
		JSONParser parser = new JSONParser();
		String s = "[0,{\"1\":{\"2\":{\"3\":{\"4\":[5,{\"6\":7}]}}}}]";
		try {
			Object obj = parser.parse(s);
			JSONArray array = (JSONArray)obj;

			System.out.println("2nd element:\t" + array.get(1) + "\n");

			JSONObject obj2 = (JSONObject)array.get(1);
			System.out.print("Field \"1\":\t\t     ");
			System.out.println(obj2.get("1"));
		} catch (ParseException e) {
			System.err.println(e.getMessage());
		}
	}
}
