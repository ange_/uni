/*
 * NEEDS JSON-simple (https://code.google.com/p/json-simple/)
 * TO WORK
 */

import org.json.simple.JSONObject;

import java.io.IOException;
import java.io.StringWriter;

public class JSONEncoding {
	public static void main(String[] args) {
		JSONObject obj = new JSONObject();

		obj.put("name", "foo");
		obj.put("num", new Integer(100));
		obj.put("balance", new Double(1000.21));
		obj.put("is_vip", true);

		System.out.println(obj);

		StringWriter out = new StringWriter();
		try {
			obj.writeJSONString(out);  // instead of String, an output file could be used
		} catch (IOException e) {
			e.printStackTrace();
		}

		String jsonText = out.toString();
		System.out.println(jsonText);
	}
}
