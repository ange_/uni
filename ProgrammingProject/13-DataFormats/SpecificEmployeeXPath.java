import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.FileInputStream;
import java.io.IOException;

public class SpecificEmployeeXPath {
	public static void main(String[] args) {
		DocumentBuilder parser = null;
		try {
			parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		Document xmlDoc = null;
		try {
			xmlDoc = parser.parse(new FileInputStream("./employees.xml"));
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}

		XPath xpath = XPathFactory.newInstance().newXPath();
		String expr = "/Employees/Employee[@emplid='2222']";
		Node employee = null;
		try {
			employee = (Node)xpath.compile(expr).evaluate(xmlDoc, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}

		if (employee != null) {
			NodeList employeeData = employee.getChildNodes();
			for (int i = 0; employeeData != null && i < employeeData.getLength(); ++i)
				if (employeeData.item(i).getNodeType() == Node.ELEMENT_NODE)
					System.out.println(employeeData.item(i).getNodeName() + ": " + employeeData.item(i).getFirstChild().getNodeValue());
		}
	}
}
