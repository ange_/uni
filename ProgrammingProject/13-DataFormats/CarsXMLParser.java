import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class CarsXMLParser {
	public static void main(String[] args) {
		DocumentBuilder builder = null;

		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		File input = new File("./cars.xml");

		Document doc = null;
		try {
			doc = builder.parse(input);
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}

		Element root = doc.getDocumentElement();
		NodeList cars = root.getChildNodes();
		for (int i = 0; i < cars.getLength(); ++i)
			System.out.println(cars.item(i).getNodeType());
	}
}
