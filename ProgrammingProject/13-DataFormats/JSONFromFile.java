import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

public class JSONFromFile {
	public static void main(String[] args) {
		JSONParser parser = new JSONParser();

		try {
			Object obj = parser.parse(new FileReader("./json_sample.json"));
			JSONObject json_object = (JSONObject)obj;
			System.out.println(json_object);

			JSONArray books = (JSONArray)json_object.get("book");
			Iterator<JSONObject> it = books.iterator();
			while (it.hasNext()) {
				JSONObject ob = it.next();
				System.out.println("\n+++ Book Entry +++");
				System.out.println("Author: " + ob.get("author"));
				System.out.println("Edition: " + ob.get("edition"));
				System.out.println("Language: " + ob.get("language"));
				System.out.println("ID: " + ob.get("id"));
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (ParseException | IOException e) {
			e.printStackTrace();
		}
	}
}
