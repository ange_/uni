import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class StudentsXMLParser {
	public static void main(String[] args) {
		DocumentBuilder builder = null;

		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		File input = new File("./students.xml");

		Document doc = null;
		try {
			doc = builder.parse(input);
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}

		NodeList student_list = doc.getElementsByTagName("student");
		for (int i = 0; i < student_list.getLength(); ++i)
			System.out.println(student_list.item(i).getAttributes().getNamedItem("rollno").getNodeValue());
	}
}
