import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;


class Vehicle {
	public String vehicleId = null;
	public String name = null;

	@Override
	public String toString() {
		return vehicleId + ": " + name;
	}
}

class Driver {
	public String driverId = null;
	public String name = null;
	public List<Vehicle> vehicles = new ArrayList<>();

	@Override
	public String toString() {
		return driverId + " | " + name + " | " + vehicles;
	}
}

class SaxHandler extends DefaultHandler {
	public List<Driver> drivers = new ArrayList<>();
	public Map<String, Vehicle> vehicles = new HashMap<>();

	private Stack<String> elementStack = new Stack<>();
	private Stack objectStack = new Stack();

	private String currentElement() {
		return elementStack.peek();
	}

	private String currentElementParent() {
		return elementStack.size() < 2 ? null : elementStack.get(elementStack.size() - 2);
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		elementStack.push(qName);
		if ("driver".equals(qName)) {
			Driver d = new Driver();
			objectStack.push(d);
			drivers.add(d);
		}
		else if ("vehicle".equals(qName))
			objectStack.push(new Vehicle());
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		elementStack.pop();

		if ("vehicle".equals(qName) || "driver".equals(qName)) {
			Object o = objectStack.pop();
			if ("vehicle".equals(qName)) {
				Vehicle v = (Vehicle)o;
				vehicles.put(v.vehicleId, v);
			}
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		String value = new String(ch, start, length).trim();

		if (value.length() == 0)
			return;

		if ("driverId".equals(currentElement())) {
			Driver d = (Driver)objectStack.peek();
			d.driverId = (d.driverId != null ? d.driverId : "") + value;
		}
		else if ("name".equals(currentElement()) && "driver".equals(currentElementParent())) {
			Driver d = (Driver)objectStack.peek();
			d.name = (d.name != null ? d.name : "") + value;
		}
		else if ("vehicleId".equals(currentElement()) && "driver".equals(currentElementParent())) {
			Driver d = (Driver) objectStack.peek();
			Vehicle v = vehicles.get(value);
			if (v != null)
				d.vehicles.add(v);
		}
		else if ("vehicleId".equals(currentElement()) && "vehicle".equals(currentElementParent())) {
			Vehicle v = (Vehicle)objectStack.peek();
			v.vehicleId = (v.vehicleId != null ? v.vehicleId : "") + value;
		}
		else if ("name".equals(currentElement()) && "vehicle".equals(currentElementParent())) {
			Vehicle v = (Vehicle)objectStack.peek();
			v.name = (v.name != null ? v.name : "") + value;
		}
	}
}

public class SaxParser {
	public static void main(String[] args) {
		SAXParser parser = null;

		try {
			parser = SAXParserFactory.newInstance().newSAXParser();
		} catch (SAXException | ParserConfigurationException e) {
			e.printStackTrace();
		}

		SaxHandler handler = new SaxHandler();
		try {
			parser.parse(new FileInputStream("/Users/ange/Documents/UniBZ/Programming Project/13-DataFormats/driverVehicleInfoSAX.xml"), handler);
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}

		for (Driver driver : handler.drivers)
			System.out.println(driver);

	}
}
