import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class csvParser {
	public static void main(String[] args) {
        String csvFile = "data.csv";
        String delimeter = ",", line = "";

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            while ((line = br.readLine()) != null) {
                String[] tokens = line.split(delimeter);

                System.out.println(Integer.parseInt(tokens[1]) + Integer.parseInt(tokens[2]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
}
