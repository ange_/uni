import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class DomParsingTest {
	public static void main(String[] args) {
		DocumentBuilder builder = null;

		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		StringBuilder xmlString = new StringBuilder("<?xml version=\"1.0\"?> <person name='Jimmy' age='42' gender='m'> </person>");
		ByteArrayInputStream input = null;
		try {
			input = new ByteArrayInputStream(xmlString.toString().getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		Document doc = null;
		try {
			doc = builder.parse(input);
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}

		Element root = doc.getDocumentElement();
		NamedNodeMap nnm = root.getAttributes();

		for (int i = 0; i < nnm.getLength(); ++i)
			System.out.println(nnm.item(i).getNodeName() + ": " + nnm.item(i).getNodeValue());
	}
}
