import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.FileInputStream;
import java.io.IOException;

public class QueryOnlyFirstLetter {
	public static void main(String[] args) {
		DocumentBuilder parser = null;
		try {
			parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		Document xmlDoc = null;
		try {
			xmlDoc = parser.parse(new FileInputStream("/Users/ange/Documents/UniBZ/Programming Project/13-DataFormats/employees.xml"));
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}

		XPath xpath = XPathFactory.newInstance().newXPath();
		String expr = "/Employees/Employee[substring(firstname, 1, 1) = 'J']/firstname";
		NodeList nL = null;
		try {
			nL = (NodeList)xpath.compile(expr).evaluate(xmlDoc, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}

		for (int i = 0; i < nL.getLength(); ++i)
			System.out.println(nL.item(i).getFirstChild().getNodeValue());
	}
}
