package homework10;

import java.io.*;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import javafx.util.Pair;

public class LinkGetter {
	/* As an idea to perform less operations, we could use a Pair<>
	 * containing the url and the list of fetched data, so if a request on the
	 * same link would have been performed, the getLinks method could just return
	 * the value of the pair. The concept of the implementation is commented in the
	 * getLinks() method (3 lines do all the job by the way).
	 */
//	private Pair<String, List<String>> website;
	
	public LinkGetter() {
		
	}
	
	public List<String> getLinks(String url) {
//		if (website != null && website.getKey() == url)
//			return website.getValue();
		
		List<String> data = null;

		try {
			URLConnection uconn = new URL(url).openConnection();
			if (! (uconn instanceof HttpURLConnection))
				throw new RuntimeException("...");

			HttpURLConnection httpconn = (HttpURLConnection)uconn;

			BufferedReader buffer = new BufferedReader(new InputStreamReader(httpconn.getInputStream()));
			String content = "";

			data = new ArrayList<>();
			while ((content = buffer.readLine()) != null) {
				Matcher m = Pattern.compile("href=\"(.*?)\"").matcher(content);
				while (m.find()) {
					String fetched_url = m.group(1);
					if (! valid(fetched_url))
						fetched_url = makeAbsolute(url, fetched_url);
					fetched_url = fetched_url.replaceAll("&amp;", "&");
					data.add(fetched_url);
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
//		website = new Pair<>(url, data);
		return data;
	}
	
	private boolean valid(String s) {
		return Pattern.matches("(http|https)://(([^.]*).){3}\\S*", s);
	}
	
	private String makeAbsolute(String url, String link) {
		if (link.charAt(0) == '/')
			return url.endsWith("/") ? url + link.substring(1) : url + link;

		else if (Character.isAlphabetic(link.charAt(0)) && (link.substring(0, 4).compareTo("http") != 0))
			return url.substring(0, url.indexOf('/') + 2) + link;

		/* Is already absolute */
		return link;
	}

	public static void main(String[] args) throws IOException {
		LinkGetter lg = new LinkGetter();
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("./links.txt")));
		lg.getLinks("https://www.google.com/").forEach(link -> {
			System.out.println(link);
			out.write(link + "\n");
		});
		out.close();
	}
}
