package homework02;

public class Auditorium extends Building {
	private int seating_capacity;
	
	public Auditorium() {
		super();
		System.out.println("Setting name, number of bathrooms and number of entrances.");
		this.seating_capacity = 0;
		System.out.println("Setting seating capacity.");
	}
	
	public Auditorium(int seating_capacity) {
		super();
		System.out.println("Setting name, number of bathrooms and number of entrances.");
		this.seating_capacity = seating_capacity;
		System.out.println("Setting seating capacity.");
	}

	public Auditorium(String name, int number_of_bathrooms, int number_of_entrances, int seating_capacity) {
		super(name, number_of_bathrooms, number_of_entrances);
		System.out.println("Setting name, number of bathrooms and number of entrances.");
		this.seating_capacity = seating_capacity;
		System.out.println("Setting seating capacity.");
	}

	public void setSeatingCapacity(int seating_capacity) {
		this.seating_capacity = seating_capacity;
	}
	

}
