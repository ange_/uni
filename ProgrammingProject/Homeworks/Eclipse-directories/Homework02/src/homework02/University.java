package homework02;
import java.util.Random;

public class University extends Building {
	private int class_rooms;
	private int meeting_rooms;
	

	public University(int class_rooms, int meeting_rooms) {
		super();
		this.class_rooms = class_rooms;
		this.meeting_rooms = meeting_rooms;
	}

	public University(String name, int number_of_bathrooms, int number_of_entrances, int class_rooms, int meeting_rooms) {
		super(name, number_of_bathrooms, number_of_entrances);
		this.class_rooms = class_rooms;
		this.meeting_rooms = meeting_rooms;
	}
	
	public University(String name, int class_rooms, int meeting_rooms) {
		super(name, new Random().nextInt(100), new Random().nextInt(20));
		System.out.println("Setting name, number of bathrooms and number of entrances.");
		this.class_rooms = class_rooms;
		System.out.println("Setting class rooms.");
		this.meeting_rooms = meeting_rooms;
		System.out.println("Setting meeting rooms.");
	}


	public void setClassMeetingRooms(int meeting_rooms, int class_rooms) {
		this.meeting_rooms = meeting_rooms;
		this.class_rooms = class_rooms;
	}
	
	@Override
	public void printBuilding() {
		super.printBuilding();
		System.out.println("Number of class rooms: " + class_rooms +
							"\nNumber of meeting rooms: " + meeting_rooms);
	}
}
