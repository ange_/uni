package homework02;
import java.util.Scanner;

public class Building {
	private String name;
	private int number_of_bathrooms;
	private int number_of_entrances;

	public Building() {
		this.name = "unnamed";
		this.number_of_bathrooms = 0;
		this.number_of_entrances = 0;
	}

	public Building(String name, int number_of_bathrooms, int number_of_entrances) {
		this.name = name;
		this.number_of_bathrooms = number_of_bathrooms;
		this.number_of_entrances = number_of_entrances;
	}

	public int getNumber_of_entrances() {
		return number_of_entrances;
	}

	public void printBuilding() {
		System.out.println("Name: " + this.name +
							"\nNumber of bathrooms: " + this.number_of_bathrooms +
							"\nNumber of entrances: " + this.number_of_entrances);
	}

	public static void main(String[] args) {
		Building[] buildings = new Building[3];

		for (int i = 0; i < buildings.length; ++i) {
			Scanner input = new Scanner(System.in);
			System.out.print("[ " + (i+1) + "/" + buildings.length + " ] Enter the name of the building: ");
			String name = input.next();

			System.out.print("[ " + (i+1) + "/" + buildings.length + " ] Enter the number of bathrooms: ");
			int number_of_bathrooms = input.nextInt();

			System.out.print("[ " + (i+1) + "/" + buildings.length + " ] Enter the number of entrances: ");
			int number_of_entrances = input.nextInt();

			buildings[i] = new Building(name, number_of_bathrooms, number_of_entrances);
		}
		
		Building with_more_entrances = buildings[0];
		for (int i = 1; i < buildings.length; ++i)
			if (buildings[i].getNumber_of_entrances() > buildings[i - 1].getNumber_of_entrances())
				with_more_entrances = buildings[i];
		
		System.out.println("The building with more entrances is: ");
		with_more_entrances.printBuilding();
	}
}
