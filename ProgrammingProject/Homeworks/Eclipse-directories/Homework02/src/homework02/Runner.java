package homework02;

public class Runner {
	public static void main(String[] args) {
		University uni = new University("Uni", 14, 52);
		uni.setClassMeetingRooms(34, 12);
		uni.printBuilding();
		
		Auditorium aud = new Auditorium("Auditorium", 13, 31, 14);
		aud.setSeatingCapacity(505);
		aud.printBuilding();
	}
}
