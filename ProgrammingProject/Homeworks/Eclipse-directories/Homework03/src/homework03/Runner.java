package homework03;

public class Runner {

	public static void main(String[] args) {
		CD it = new CD("Black Ice", "01234", "AC/DC", 14.90, 3);
		it.print();
		
		DVD it2 = new DVD("The day after tomorrow", "09876", "Unknown Author", 19.99, 200);
		it2.print();
		
		Book it3 = new Book("Così parlò Zarathustra", "5491", "F. Nietzsche", 15.00);
		it3.print();
	}

}
