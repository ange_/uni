package homework03;

public class DVD extends Item {
	
	private int timeDuration;

	public DVD() {
		super();
		timeDuration = 0;
	}

	public DVD(String name, String code, String author, double price, int timeDuration) {
		super(name, code, author, price);
		this.timeDuration = timeDuration;
	}

	@Override
	public void print() {
		System.out.println(this);
		System.out.println("\tTime duration: " + timeDuration);
	}
}
