package homework03;

public class Book extends Item {

	public Book() {
		super();
	}

	public Book(String name, String code, String author, double price) {
		super(name, code, author, price);
	}

	public void print() {
		System.out.println("\tAuthor: " + super.author);
	}
}
