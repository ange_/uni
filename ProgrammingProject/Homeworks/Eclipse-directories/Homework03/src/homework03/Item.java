package homework03;

import java.text.DecimalFormat;
import java.util.Currency;
import java.util.Locale;

public class Item {
	private String name;
	private String code;
	protected String author;
	private double price;

	public Item() {
		name = "";
		code = "";
		author = "";
		price = 0;
	}

	public Item(String name, String code, String author, double price) {
		this.name = name;
		this.code = code;
		this.author = author;
		this.price = price;
	}

	public void print() {
		System.out.println(this);
	}

	@java.lang.Override
	public java.lang.String toString() {
		DecimalFormat df = new DecimalFormat("#.00");
		df.setCurrency(Currency.getInstance(Locale.getDefault()));
		return "Item name: " + name +
				"\n\tPrice: " + DecimalFormat.getCurrencyInstance().format(price);
	}
}
