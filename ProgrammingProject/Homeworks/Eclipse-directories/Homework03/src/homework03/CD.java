package homework03;

public class CD extends Item {
	private int numberOfSongs;

	public CD() {
		super();
		this.numberOfSongs = 0;
	}

	public CD(String name, String code, String author, double price, int numberOfSongs) {
		super(name, code, author, price);
		this.numberOfSongs = numberOfSongs;
	}

	@Override
	public void print() {
		// Thanks to the toString() method, a simple System.out.println(this)
		// would be perfect, but since here we must override the print() method...
		System.out.println(this);
		System.out.println("\tNumber of songs: " + numberOfSongs);
	}
}
