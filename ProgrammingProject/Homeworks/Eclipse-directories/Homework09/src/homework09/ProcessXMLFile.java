package homework09;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/* Support class to represent an employee.
 * If the program needed more control on it,
 * it would be a much better to create a single java file for it.
 */
class Employee {
	private String name;
	private int age;
	private String address;

	public Employee(String name, int age, String address) {
		this.name = name;
		this.age = age;
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public String getAddress() {
		return address;
	}

	@Override
	public String toString() {
		return "Name: " + name +
				"\nAge: " + age +
				"\nAddress: " + address;
	}
}

/* Support class to return two values from a single method
 * without a list
 */
class ValuePair<N extends Number, U extends Number> {
	private final N firstValue;
	private final U secondValue;

	public ValuePair(N first, U second) {
		this.firstValue = first;
		this.secondValue = second;
	}

	public N getFirstValue() {
		return firstValue;
	}

	public U getSecondValue() {
		return secondValue;
	}

	@Override
	public String toString() {
		return firstValue + "\t" + secondValue;
	}
}


class EmployeeNotFoundException extends Exception {
	public EmployeeNotFoundException(String message) {
		super(message);
	}
}


public class ProcessXMLFile {
	private String xml_filepath;

	public ProcessXMLFile(String xml_filepath) {
		this.xml_filepath = xml_filepath;
	}

	/**
	 * @return Return the total number of employees from the xml file
	 */
	public int getTotalNoOfEmployees() {
		DocumentBuilder parser = null;

		try {
			parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		Document xmlDoc = null;
		try {
			xmlDoc = parser.parse(new FileInputStream(xml_filepath));
		} catch (IOException | SAXException e) {
			e.printStackTrace();
		} // FileNotFound is a subclass of IOException and now we do not need to manage them.

		XPath xpath = XPathFactory.newInstance().newXPath();
		String expr = "count(/root/element)";
		Number result = null;

		try {
			result = (Number)xpath.compile(expr).evaluate(xmlDoc, XPathConstants.NUMBER);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}

		return result.intValue();
	}


	/**
	 * @return A ValuePair where the first value is the Minimal age and the second value is the Maximal age
	 */
	public ValuePair<Integer, Integer> getEmployeeAgeRange() {
		DocumentBuilder parser = null;

		try {
			parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		Document xmlDoc = null;
		try {
			xmlDoc = parser.parse(new FileInputStream(xml_filepath));
		} catch (IOException | SAXException e) {
			e.printStackTrace();
		} // FileNotFound is a subclass of IOException and now we do not need to manage them.

		XPath xpath = XPathFactory.newInstance().newXPath();
		String expr = "/root/element/age";
		NodeList employees = null;

		try {
			employees = (NodeList)xpath.compile(expr).evaluate(xmlDoc, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}

		int min, max = min = 0;
		for (int i = 0; i < employees.getLength(); ++i) {
			int current_value = Integer.parseInt(employees.item(i).getFirstChild().getNodeValue());
			if (i == 0)
				max = min = current_value;
			else if (current_value > max)
				max = current_value;
			else if (current_value < min)
				min = current_value;
		}

		return new ValuePair<Integer, Integer>(min, max);
	}


	/**
	 * @param The email of the employee to search
	 * @return An Employee object having name, age and address
	 */
	Employee getSingleEmpRecordByEmail(String email) throws EmployeeNotFoundException {
		DocumentBuilder parser = null;

		try {
			parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		Document xmlDoc = null;
		try {
			xmlDoc = parser.parse(new FileInputStream(xml_filepath));
		} catch (IOException | SAXException e) {
			e.printStackTrace();
		} // FileNotFound is a subclass of IOException and now we do not need to manage them.

		XPath xpath = XPathFactory.newInstance().newXPath();
		String expr = "/root/element[email = 'simmonsmoran@toyletry.com']";
		Node employee = null;

		try {
			employee = (Node)xpath.compile(expr).evaluate(xmlDoc, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}

		/* Data could be put directly in a List and returned at the end
		 * but this would lead in an undefined position case (e.g. sometimes address could be
		 * 		before age or other times name can come as last element).
		 */
		String emp_name, emp_address = emp_name = null;
		int emp_age = 0;

		if (employee != null) {
			NodeList employee_data = employee.getChildNodes();
			for (int i = 0; i < employee_data.getLength(); ++i)
				if (employee_data.item(i).getNodeType() == Node.ELEMENT_NODE) {
					switch(employee_data.item(i).getNodeName()) {
						case "name":
							emp_name = employee_data.item(i).getFirstChild().getNodeValue();
							break;
						case "age":
							emp_age = Integer.parseInt(employee_data.item(i).getFirstChild().getNodeValue());
							break;
						case "address":
							emp_address = employee_data.item(i).getFirstChild().getNodeValue();
							break;
					}
				}

		}
		else
			throw new EmployeeNotFoundException(email + " employee not found.");

		return new Employee(emp_name, emp_age, emp_address);
	}


	/**
	 * @param The id of the employee to search
	 * @return A List<String> of friends corresponding the id employee. If employee is not found, EmployeeNotFoundException is thrown.
	 */
	public List<String> getFriendsListOfEmployee(String id) throws EmployeeNotFoundException {
		DocumentBuilder parser = null;

		try {
			parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		Document xmlDoc = null;
		try {
			xmlDoc = parser.parse(new FileInputStream(xml_filepath));
		} catch (IOException | SAXException e) {
			e.printStackTrace();
		} // FileNotFound is a subclass of IOException and now we do not need to manage them.

		XPath xpath = XPathFactory.newInstance().newXPath();
		String expr = "/root/element[_id = '" + id + "']/friends/felement/fname";
		NodeList friends_list = null;

		try {
			friends_list = (NodeList)xpath.compile(expr).evaluate(xmlDoc, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}

		if (friends_list == null)
			throw new EmployeeNotFoundException(id + " employee not found.");

		List<String> friends = new ArrayList<>();
		for (int i = 0; i < friends_list.getLength(); i++)
			friends.add(friends_list.item(i).getFirstChild().getNodeValue());

		return friends;
	}


	/**
	 * @return A ValuePair where the first value is the active users counter and the second value the inactive one.
	 */
	public ValuePair<Integer, Integer> getActiveAndInactiveEmpCount() {
		DocumentBuilder parser = null;

		try {
			parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		Document xmlDoc = null;
		try {
			xmlDoc = parser.parse(new FileInputStream(xml_filepath));
		} catch (IOException | SAXException e) {
			e.printStackTrace();
		} // FileNotFound is a subclass of IOException and now we do not need to manage them.

		XPath xpath = XPathFactory.newInstance().newXPath();
		String expr = "/root/element/isActive";
		NodeList employees = null;

		try {
			employees = (NodeList)xpath.compile(expr).evaluate(xmlDoc, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}

		int active_users, inactive_users = active_users = 0;
		for (int i = 0; i < employees.getLength(); ++i)
			if (Boolean.parseBoolean(employees.item(i).getFirstChild().getNodeValue()))
				++active_users;
			else
				++inactive_users;

		return new ValuePair<Integer, Integer>(active_users, inactive_users);
	}
}