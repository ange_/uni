package homework09;

import java.util.List;

public class Runner {

	public static void main(String[] args) {
		ProcessXMLFile file = new ProcessXMLFile(Runner.class.getResource("/sample.xml").getFile());
		/* Since the jar file would become the root directory,
		 * Resources becomes the root directory for getResource(). */
		System.out.println("Total Number of Employees: " + file.getTotalNoOfEmployees());
		System.out.println("----------------------");

		ValuePair<Integer, Integer> age_range = file.getEmployeeAgeRange();
		System.out.println("Minimal age: " + age_range.getFirstValue() + " and Maximal age: " + age_range.getSecondValue());
		System.out.println("----------------------");

		String email = "simmonsmoran@toyletry.com";
		System.out.println("Searching for employee by email " + email + "...");
		try {
			System.out.println(file.getSingleEmpRecordByEmail(email));
		} catch (EmployeeNotFoundException e) {
			System.err.println("Employee with email <" + email + "> not found.");
		}
		System.out.println("----------------------");

		String id = "5ad218d789987a94aac57445";
		try {
			List<String> employee_friends = file.getFriendsListOfEmployee(id);
			System.out.println("Searching for employee by id " + id + "...");
			for (String friend : employee_friends)
				System.out.println("Name of friend: " + friend);
		} catch (EmployeeNotFoundException e) {
			System.err.println("Employee with id: " + id + " not found.");
		}
		System.out.println("----------------------");

		ValuePair<Integer, Integer> act_inact_employees = file.getActiveAndInactiveEmpCount();
		System.out.println("Active employees: " + act_inact_employees.getFirstValue() + " and Inactive employees: " + act_inact_employees.getSecondValue());
	}

}
