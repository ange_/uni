package homework08;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class Server {
	private double answerProbability;
	
	public Server() {
		answerProbability = Math.random();
	}
	
	public Server(double answerProbability) {
		this.answerProbability = answerProbability;
	}
	
	public String getLastMessage() throws UnavailableServerException {
		double rand = Math.random();
		if (rand < answerProbability)
			return "Message";
		throw new UnavailableServerException();
	}
	
	@Override
	public String toString() {
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setDecimalSeparator('.');
		DecimalFormat df = new DecimalFormat("0.0", dfs);
		return df.format(answerProbability);
	}
}
