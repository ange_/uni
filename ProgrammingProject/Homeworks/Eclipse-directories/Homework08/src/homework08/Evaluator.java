package homework08;

public class Evaluator {
	public final static int nbOfRetrievals = 8;
	
	public double evaluateSuccessRate(Server s, Client c) {
		double success_rate = 0;

		for (int i = 0; i < nbOfRetrievals; ++i)
			try {
				c.retrieveMessage(s);
				++success_rate;
			} catch (MessageRetrievalException ex) {
				// Error handling not needed
			}
		
		return success_rate/nbOfRetrievals;
	}
}
