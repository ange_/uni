package homework08;

public class Client {
	private int nbOfRetries;
	private String msg;
	
	public Client() {
		nbOfRetries = 0;
		msg = null;
	}
	
	public Client(int nbOfRetries) {
		this.nbOfRetries = nbOfRetries;
	}
	
	public String retrieveMessage(Server s) throws MessageRetrievalException {
		msg = null;
		for (int i = 0; i < nbOfRetries; ++i)
			try {
				msg = s.getLastMessage();
				break;
			} catch (UnavailableServerException e) {
				continue;
			}
		
		if (msg == null)
			throw new MessageRetrievalException();
	
		return msg;
	}
	
	@Override
	public String toString() {
		return String.valueOf(nbOfRetries);
	}
}
