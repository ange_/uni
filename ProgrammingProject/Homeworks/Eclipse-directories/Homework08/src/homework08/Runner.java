package homework08;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Runner {
	public static void main(String[] args) {
		List<Server> servers = new ArrayList<>();
		List<Client> clients = new ArrayList<>();
		
		Random r = new Random();
		int n = r.nextInt(5) + 1;
		for (int i = 0; i < n; ++i)
			servers.add(new Server(r.nextDouble()));
		
		n = r.nextInt(5) + 1;
		for (int i = 0; i < n; ++i)
			clients.add(new Client(r.nextInt(10) + 2));
		
		Evaluator ev = new Evaluator();
		
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setDecimalSeparator('.');
		DecimalFormat df = new DecimalFormat("0.00", dfs);
		for (Server server : servers)
			for (Client client : clients)
				System.out.println("Client-" + client + " with Server-" + server + ": " + df.format(ev.evaluateSuccessRate(server, client)));
	}
}
