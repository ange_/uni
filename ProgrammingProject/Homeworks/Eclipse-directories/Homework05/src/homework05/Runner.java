package homework05;

public class Runner {

    public static void main(String[] args) {
    
            Object monitor = new Object();
            PriorityThread t1 = new PriorityThread(monitor);
            PriorityThread t2 = new PriorityThread(monitor);
            PriorityThread t3 = new PriorityThread(monitor);
            PriorityThread t4 = new PriorityThread(monitor);
            
    
            Thread th1 = new Thread(t1, "HI1");
            th1.setPriority(10); //maximum priority
            Thread th2 = new Thread(t2, "HI2");
            th2.setPriority(10); //minimum priority
            
            Thread th3 = new Thread(t3, "MID");
            th3.setPriority(5);
            
            Thread th4 = new Thread(t4, "LOW");
            th4.setPriority(1); //minimum priority

            th1.start();
            th2.start();
            try {
                    th1.join();
                    th2.join();
            } catch (InterruptedException ex) {
                    System.err.println("Unexpected interruption.");
            }
            th3.start();
            th4.start();
    }
}