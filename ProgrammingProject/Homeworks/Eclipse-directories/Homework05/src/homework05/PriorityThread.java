package homework05;

public class PriorityThread implements Runnable {
    final private Object monitor;

    @Override
    public void run() {
            long start = System.currentTimeMillis();

            synchronized (monitor) {
                    for (int i = 0; i < 5; ++i)
                            System.out.print(Thread.currentThread().getName() + ": " + i + "\t");

                    System.out.println();
                    monitor.notifyAll();
            }

            System.out.println("I am thread " + Thread.currentThread().getName() + " and I'm done. " +
                            "I took " + (System.currentTimeMillis() - start) + " ms to execute");
    }

    public PriorityThread(Object monitor) {
            this.monitor = monitor;
    }
}