package homework06;


public class Runner {

	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		int num1 = 10, num2 = 20;
		CheckPrimeNumberThread cpnt = new CheckPrimeNumberThread();

		for (int i = num1; i < num2; ++i) {
			Thread t = new Thread(cpnt);
			t.start();
			cpnt.isPrime(i);
		}

		System.out.println("<<<<<<<<<< Prime Numbers >>>>>>>>>>");
		System.out.println(cpnt.getPrimes());
		System.out.println("<<<<<<<<<< Non-Prime Numbers >>>>>>>>>>");
		System.out.println(cpnt.getNonPrimes());
		System.out.println("<<<<<<<<<< Execution Time >>>>>>>>>>");
		System.out.println(System.currentTimeMillis() - start + " ms");
	}

}
