package homework06;

import java.util.List;

public interface CheckNumber<N extends Number> {
	void isPrime(int n);
	List<N> getPrimes();
	List<N> getNonPrimes();
}
