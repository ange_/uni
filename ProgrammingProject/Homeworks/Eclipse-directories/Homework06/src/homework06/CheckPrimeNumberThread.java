package homework06;

import java.util.ArrayList;
import java.util.List;

public class CheckPrimeNumberThread implements Runnable, CheckNumber<Number> {
	private List<Number> primes = new ArrayList<>();
	private List<Number> nonPrimes = new ArrayList<>();
	
	@Override
	public synchronized void isPrime(int n) {
		for (int i = 2; i < (n/2); ++i)
			if (n % i == 0) {
				nonPrimes.add(n);
				return;
			}

		primes.add(n);
	}

	@Override
	public List<Number> getPrimes() {
		return primes;
	}

	@Override
	public List<Number> getNonPrimes() {
		return nonPrimes;
	}

	@Override
	public void run() {

	}

}
