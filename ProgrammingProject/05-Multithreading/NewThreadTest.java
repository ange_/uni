class T1 implements Runnable {
	@Override
	public void run() {
		System.out.println("1. There must be some way out of here");
		System.out.println("said the joker to the thief");
		System.out.println("There’s too much confusion");
		System.out.println("I can’t get no relief");
	}
}

class T2 implements Runnable {
	public int result = 0;
	@Override
	public void run() {
		System.out.println("2. Businessmen, they drink my wine");
		System.out.println("plowmen dig my earth");
		System.out.println("None of them along the line");
		System.out.println("know what any of it is worth");
	}
}

public class NewThreadTest {
	public static void main(String[] args) throws InterruptedException {
		Thread tt1 = new Thread(new T1());
		Thread tt2 = new Thread(new T2());
		tt1.start();
		tt1.join();
		tt2.start();
	}
}
