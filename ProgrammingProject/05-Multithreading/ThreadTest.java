import java.util.Random;

public class ThreadTest implements Runnable {
	@Override
	public void run() {
		final int n = new Random().nextInt(5000) + 1;
		System.out.println("[ Thread ] Begin ");
		System.out.println(n);
		try {
			Thread.sleep(n);
			System.out.println("M1");
			Thread.sleep(n);
			System.out.println("M2");
			Thread.sleep(n);
			System.out.println("[ Thread ] End");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		ThreadTest tt1 = new ThreadTest();
		ThreadTest tt2 = new ThreadTest();

		tt1.run();
		tt2.run();
		System.out.println("End of tt1 && tt2");

		Thread t1 = new Thread(tt1);
		Thread t2 = new Thread(tt2);
		t1.start();
		t2.start();
	}
}