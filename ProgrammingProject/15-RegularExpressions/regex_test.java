import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class regex_test {
	public static void main(String[] args) {
		Pattern pattern = Pattern.compile("(\\d+)");
		Matcher m = pattern.matcher("This watch costs 215 dollars and 99 cents.");

		System.out.println(m.replaceAll("\\$$1\\$"));
	}
}
