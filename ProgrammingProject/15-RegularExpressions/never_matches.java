import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class never_matches {
	public static void main(String[] args) {
		String s = "Never gonna give you up\n" + "Never gonna let you down\n" +
				"Never gonna run around and desert you\n" + "Never gonna make you cry\n" +
				"Never gonna say goodbye\n" +
				"Never gonna tell a lie and hurt you";

		Pattern p = Pattern.compile("Never\\s\\w+\\s\\w+");
		Matcher m = p.matcher(s);

		while (m.find())
			System.out.println(String.format("From %d to %d: %s", m.start(), m.end(), s.substring(m.start(), m.end())));
	}
}
