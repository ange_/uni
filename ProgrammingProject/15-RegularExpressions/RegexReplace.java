import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexReplace {
	private static String REGEX = "dog";
	private static String INPUT = "The dog says meow. " + "All dogs say meow.";
	private static String REPLACE = "cat";

	public static void main(String[] args) {
		Matcher m = Pattern.compile(REGEX).matcher(INPUT);
		System.out.println(m.replaceAll(REPLACE));
	}
}
