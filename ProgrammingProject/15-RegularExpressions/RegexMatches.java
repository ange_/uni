import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexMatches {
	// Count number of times "cat" appears.
	private static final String REGEX = "\\bcat\\b";
	private static final String INPUT = "cat cat cat cattie cat";

	public static void main(String[] args) {
		Pattern p = Pattern.compile(REGEX);
		Matcher m = p.matcher(INPUT);

		int counter = 0;
		while (m.find()) {
			++counter;
			System.out.println("Match number: " + counter);
			System.out.println("\tstart: " + m.start());
			System.out.println("\tend: " + m.end());
		}

		System.out.println("### Appears " + counter + " times");
	}
}
