import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class regex_test2 {
	public static void main(String[] args) {
		String s = "The price is 100.00 eur";
		String ptrn = "(\\w+)\\s+is\\s((\\d+)\\.(\\d+)\\s+eur)";

		Pattern p = Pattern.compile(ptrn);
		Matcher m = p.matcher(s);
		if (m.find())
			for (int i = 0, n_groups = m.groupCount(); i <= n_groups; i++)
				System.out.println(String.format("Group %d: %s", i, m.group(i)));
	}
}
