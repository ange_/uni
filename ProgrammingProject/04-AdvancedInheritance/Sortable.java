interface Sortable {
	boolean isBiggerThan(Sortable obj);
}