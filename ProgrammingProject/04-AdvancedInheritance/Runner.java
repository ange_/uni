public class Runner {
	public static void main(String[] args) {
		Building b = new Building(), b2 = new Building();
		b.setHeight(324.3);
		b2.setHeight(23.4);

		if (b.isBiggerThan(b2))
			System.out.println("b is bigger than b2");
	}
}