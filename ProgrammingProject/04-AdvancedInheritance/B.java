class A {
	public void methodA() {
		System.out.println("A");
	}
}

public class B extends A {
	public void methodB() {
		System.out.println("B");
	}

	public static void main(String[] args) {
		B b = new B();
		A a = new A();

		a.methodA();
		b.methodA();
		b.methodB();

		A a2 = new B();
		a2.methodA();


		A a3 = new B(); //we don't have access to methodB() a3.methodA();
		B b2 = (B) a3;
		b2.methodB(); //downcasting
		B b3 = (B) new A();
		b3.methodA();
		b3.methodB(); //runtime error
	}
}