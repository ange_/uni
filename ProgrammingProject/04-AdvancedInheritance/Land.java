public class Land {
	private int w;
	private int l;

	public double getArea() {
		return w * l;
	}

	public Land(int w, int l) {
		this.w = w;
		this.l = l;
	}

	@Override
	public boolean equals(Object obj) {
		return this.getArea() == ((Land)obj).getArea();
	}

	public static void main(String[] args) {
		Land l1 = new Land(10, 10), l2 = new Land(20, 5);

		System.out.println(l1.equals(l2));
	}
}