public interface Clearable extends Drawable {
	void clear();
}
