public interface DrawablePrintableClearable extends Drawable, Printable {
	void clear();
}
