public class Building implements Sortable {
	private double height;

	public void setHeight(double height) {
		this.height = height;
	}

	public double getHeight() {
		return height;
	}

	@Override
	public boolean isBiggerThan(Sortable obj) {
		Building oobj = (Building)obj;
		return this.height > oobj.getHeight();
	}
}