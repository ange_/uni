public class WindowDecorator implements Window {
	protected Window windowToDecorate;

	public WindowDecorator(Window windowToDecorate) {
		this.windowToDecorate = windowToDecorate;
	}

	public void draw() {
		windowToDecorate.draw();
	}

	public String getDescription() {
		return windowToDecorate.getDescription();
	}
}
