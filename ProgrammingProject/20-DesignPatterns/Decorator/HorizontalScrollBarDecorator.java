public class HorizontalScrollBarDecorator extends WindowDecorator {
	public HorizontalScrollBarDecorator(Window windowToDecorate) {
		super(windowToDecorate);
	}

	private void drawHorizontalScrollbar() {
		// Do stuff
	}

	@Override
	public void draw() {
		super.draw();
		drawHorizontalScrollbar();
	}

	@Override
	public String getDescription() {
		return super.getDescription() + ", including horizontal scrollbars";
	}
}
