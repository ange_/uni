public class AirConditionCarDecorator extends CarDecorator {
	public AirConditionCarDecorator(Car toDecorate) {
		super(toDecorate);
	}

	@Override
	public String getDescription() {
		return super.getDescription() + " + aircondition";
	}
}