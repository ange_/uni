public class RadioCarDecorator extends CarDecorator {
	public RadioCarDecorator(Car toDecorate) {
		super(toDecorate);
	}

	@Override
	public String getDescription() {
		return super.getDescription() + " + radio";
	}
}