public class CarDemo {
	public static void main(String[] args) {
		Car c = new RadioCarDecorator(new AirConditionCarDecorator(new SimpleCar())); 
		System.out.println(c.getDescription());

		c = new RadioCarDecorator(new SimpleCar()); 
		System.out.println(c.getDescription());

		c = new AirConditionCarDecorator(new SimpleCar()); 
		System.out.println(c.getDescription());

		c = new SimpleCar(); 
		System.out.println(c.getDescription());
	}
}