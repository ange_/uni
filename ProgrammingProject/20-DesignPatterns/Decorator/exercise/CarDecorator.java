public class CarDecorator implements Car {
	protected Car c;

	public CarDecorator(Car toDecorate) {
		c = toDecorate;
	}

	public String getDescription() {
		return c.getDescription();
	}
}