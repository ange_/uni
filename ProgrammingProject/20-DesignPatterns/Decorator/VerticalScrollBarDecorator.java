public class VerticalScrollBarDecorator extends WindowDecorator {
	public VerticalScrollBarDecorator(Window windowToDecorate) {
		super(windowToDecorate);
	}

	private void drawVerticalScrollbar() {
		// Draw stuff
	}

	@Override
	public void draw() {
		super.draw();
		drawVerticalScrollbar();
	}

	@Override
	public String getDescription() {
		return super.getDescription() + ", including vertical scrollbars";
	}
}