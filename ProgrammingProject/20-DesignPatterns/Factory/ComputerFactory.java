public class ComputerFactory {
	public Computer getComputer(String type) {
		switch (type) {
			case "Laptop":
				return new Laptop();
			case "Server":
				return new Server();
		}

		return null;
	}
}
