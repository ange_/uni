public class Runner {
	public static void main(String[] args) {
		Computer laptop_f = new ComputerFactory().getComputer("Laptop");
		Computer server_f = new ComputerFactory().getComputer("Server");
		laptop_f.describe();
		server_f.describe();

		Computer laptop = new Laptop();
		Computer server = new Server();
		laptop.describe();
		server.describe();
	}
}
