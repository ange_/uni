public class SingleObjectDemo {
	public static void main(String[] args) {
		// Compilation error, ctor is private
		// SingleObject obj = new SingleObject();

		SingleObject obj = SingleObject.getInstance();
		obj.showMessage();
	}
}
