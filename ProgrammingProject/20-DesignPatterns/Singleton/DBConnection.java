public class DBConnection {
	private static DBConnection instance = new DBConnection();
	private String lastQuery = "";

	private DBConnection() { }

	public static DBConnection getInstance() {
		return instance;
	}

	public void query(String query) {
		lastQuery = query;
	}

	public String getLastQuery() {
		return lastQuery;
	}
}
