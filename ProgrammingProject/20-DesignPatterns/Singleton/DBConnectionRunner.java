public class DBConnectionRunner {
	public static void methodA() {
		DBConnection db = DBConnection.getInstance();
		db.query("Useless stuff");
	}

	public static void methodB() {
		System.out.println("Really Awesome, it prints out what saved in the variable.");
		System.out.print("\tIs also static, so everytime is called it will print out: ");
		System.out.println(DBConnection.getInstance().getLastQuery());
		System.out.println("Unexpected, is it?");
	}

	public static void main(String[] args) {
		methodA();
		methodB();
	}
}
