public class CPU {
	public void freeze() {
		System.out.println("Freezing...");
	}
	public void jump() {
		System.out.println("Jumping...");
	}
	public void execute() {
		System.out.println("Executing...");
	}
}