public class ComputerFacade {
	private CPU processor;
	private HardDrive HDD;
	private Memory RAM;

	public ComputerFacade() {
		this.processor = new CPU();
		this.HDD = new HardDrive();
		this.RAM = new Memory();
	}

	public void start() {
		processor.freeze();
		final long BOOT_ADDRESS = 0x0;
		final int SECTOR_SIZE = 512;
		final long BOOT_SECTOR = 0;
		RAM.load(BOOT_ADDRESS, HDD.read(BOOT_SECTOR, SECTOR_SIZE));
		processor.jump();
		processor.execute();
	}
}