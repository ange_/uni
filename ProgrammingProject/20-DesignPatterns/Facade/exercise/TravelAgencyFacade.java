public class TravelAgencyFacade {
	String origin = null;

	public TravelAgencyFacade(String s) {
		this.origin = s;
	}

	public void bookTrip(String destination, String from, String to) { FlightBooker fb = new FlightBooker();
		fb.book(origin, destination, from);
		HotelBooker hb = new HotelBooker();
		hb.book(destination, from, to);
		fb.book(destination, origin, to);
	}
}