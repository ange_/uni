public class Connection {
	Plug p = null;
	Socket s = null;

	public void connect(Plug p, Socket s) {
		this.s = s;

		if (s instanceof UKSocket)
			this.p = (p instanceof UKPlug) ? p : new ItalianToUKPlug(p);
		else if (s instanceof ItalianSocket)
			this.p = (p instanceof ItalianPlug) ? p : new UKToItalianPlug(p);
	}

	public boolean checkConnection() {
		return (this.s instanceof UKSocket && (this.p instanceof UKPlug || this.p instanceof ItalianToUKPlug)) || (this.s instanceof ItalianSocket && (this.p instanceof ItalianPlug || this.p instanceof UKToItalianPlug)) ? true : false;
	}
}