public class Runner {
	public static void main(String[] args) {
		Plug ukp = new UKPlug();
		Socket uks = new UKSocket();
		Plug ip = new ItalianPlug();
		Socket is = new ItalianSocket();
		Connection c = new Connection(); c.connect(ukp,is);
		System.out.println(c.checkConnection());

		c.connect(ukp,uks);
		System.out.println(c.checkConnection());
		c.connect(ip,uks);
		System.out.println(c.checkConnection());
		c.connect(ip,is);
		System.out.println(c.checkConnection());
	}
}