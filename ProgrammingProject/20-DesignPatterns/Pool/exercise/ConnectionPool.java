import java.util.ArrayList;

class ConnectionPool {
	private ArrayList<Connection> free = new ArrayList<>();
	private ArrayList<Connection> busy = new ArrayList<>();

	public ConnectionPool(int nConnections) {
		for (int i = 0; i < nConnections; ++i)
			free.add(new Connection());
	}

	public Connection getConnection() {
		if (free.size() > 0) {
			Connection c = free.get(0);
			busy.add(c);
			free.remove(c);

			return c;
		}

		return null;
	}

	public void releaseConnection(Connection c) {
		free.add(c);
		busy.remove(c);
	}
}
