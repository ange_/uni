public class Processor implements Runnable {
	String name = "";
	ConnectionPool cp = null;
	int computationTime = 0;
	
	public Processor(String name, ConnectionPool cp, int computationTime) {
		super();
		this.name = name;
		this.cp = cp;
		this.computationTime = computationTime;
	}

	@Override
	public void run() {
		try {
			Connection c = null;
			for (int i = 0; i < 5; i++) {
				c = cp.getConnection();

				while (c == null) {
					System.out.println(name + " waiting for a free connection.");
					Thread.sleep(93);
					c = cp.getConnection();
				}

				Thread.sleep(computationTime);
				c.sendResults("some result at " + i, name);
				cp.releaseConnection(c);
			}
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
	}
}