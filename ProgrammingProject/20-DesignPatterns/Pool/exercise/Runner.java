public class Runner {
	public static void main(String[] args) {
		ConnectionPool cp = new ConnectionPool(1);

		Processor p1 = new Processor("P1", cp, 500);
		Processor p2 = new Processor("P2", cp, 1000);
		Processor p3 = new Processor("P3", cp, 1500);

		Thread t1 = new Thread(p1);
		Thread t2 = new Thread(p2);
		Thread t3 = new Thread(p3);

		t1.start();
		t2.start();
		t3.start();
	}
}