public class Worker {
	public int id;

	public Worker() { }
	public Worker(int id) {
		this.id = id;
	}

	public void doJob() {
		System.out.println("Worker " + id + " is working...");
	}
}
