public class WorkerPoolDemo {
	public static void main(String[] args) {
		WorkerPool pool = new WorkerPool(5);
		
		Worker w1 = pool.getWorker();
		System.out.println(pool.numberOfFreeWorkers());

		Worker w2 = pool.getWorker();
		System.out.println(pool.numberOfFreeWorkers());

		Worker w3 = pool.getWorker();
		System.out.println(pool.numberOfFreeWorkers());

		w2.doJob();
		pool.releaseWorker(w2);
		System.out.println(pool.numberOfFreeWorkers());

		w3.doJob();
		pool.releaseWorker(w3);
		System.out.println(pool.numberOfFreeWorkers());
	}
}
