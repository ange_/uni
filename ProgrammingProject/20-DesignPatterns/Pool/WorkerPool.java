import java.util.ArrayList;

public class WorkerPool {
	private ArrayList<Worker> free = new ArrayList<>();
	private ArrayList<Worker> busy = new ArrayList<>();

	WorkerPool(int n) {
		for (int i = 0; i < n; ++i)
			free.add(new Worker(i + 1));
	}

	public Worker getWorker() {
		if (free.size() > 0) {
			Worker w = free.get(0);
			busy.add(w);
			free.remove(w);
			return w;
		}

		return null;
	}

	public void releaseWorker(Worker w) {
		free.add(w);
		busy.remove(w);
	}

	public int numberOfFreeWorkers() {
		return free.size();
	}
}
