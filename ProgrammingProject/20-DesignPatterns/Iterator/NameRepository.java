public class NameRepository implements Container {
	public String[] names = {"John", "Frank", "Bob"};

	@Override
	public CustomIterator getIterator() {
		return new NameIterator(names);
	}
}