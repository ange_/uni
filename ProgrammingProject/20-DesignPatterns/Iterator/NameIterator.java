public class NameIterator implements CustomIterator {
	private String[] names;
	private int index = 0;

	public NameIterator(String[] names) {
		this.names = names;
	}

	@Override
	public boolean hasNext() {
		return (index < names.length);
	}

	@Override
	public Object next() {
		return hasNext() ? names[index++] : null;
	}
}