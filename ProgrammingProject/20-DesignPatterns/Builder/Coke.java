public class Coke extends ColdDrink {
	@Override
	public float price() {
		return 30.8f;
	}

	@Override
	public String name() {
		return "Coke";
	}
}
