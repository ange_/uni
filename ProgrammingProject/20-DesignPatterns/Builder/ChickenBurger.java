public class ChickenBurger extends Burger {
	@Override
	public float price() {
		return 25.7f;
	}

	@Override
	public String name() {
		return "Chicken Burger";
	}
}
