import java.util.List;
import java.util.ArrayList;

public class Meal {
	private List<Item> items = new ArrayList<>();

	public void addItem(Item item) {
		items.add(item);
	}

	public float getCost() {
		float price = 0.0f;
		
		for (Item item : items)
			price += item.price();

		return price;
	}

	public void showItems() {
		for (Item item : items)
			System.out.printf("Item: %s, Price: %.2f, Packaging: %s\n", item.name(), item.price(), item.packing().pack());
	}
}
