import java.util.ArrayList;

public class MyObservable {
	private ArrayList<MyObserverInterface> listOfObservers = new ArrayList<>();

	public void addObserver(MyObserverInterface observer) {
		listOfObservers.add(observer);
	}

	public void removeObserver(MyObserverInterface observer) {
		listOfObservers.remove(observer);
	}

	public void notifyObservers() {
		for (MyObserverInterface observer : listOfObservers)
			observer.notifyMe();
	}
}