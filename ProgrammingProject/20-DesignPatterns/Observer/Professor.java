public class Professor implements MyObserverInterface {
	private MyObservable observing_obj = null;

	public Professor(MyObservable observing_obj) {
		this.observing_obj = observing_obj;
		observing_obj.addObserver(this);
	}

	@Override
	public void notifyMe() {
		System.out.println("I am professor. A new message is on OLE: " + ((OLE)observing_obj).latestMessage);
	}
}