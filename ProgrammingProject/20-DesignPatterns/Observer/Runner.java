public class Runner {
	public static void main(String[] args) {
		OLE ole = new OLE();
		
		Student s1 = new Student(ole);
		Student s2 = new Student(ole);
		Professor p = new Professor(ole);

		ole.latestMessage = "The slides for the lecture are online."; ole.notifyObservers();
	}
}