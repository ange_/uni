class MyLinkedListException extends Exception { }
class InvalidIndexException extends MyLinkedListException {
	public int index = 0;

	public InvalidIndexException(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}
}

class ObjectNotFoundException extends MyLinkedListException { }
class EmptyListException extends ObjectNotFoundException { }

public class MyLinkedList {
	public int length = 0;

	public Object objectAt(int index) throws InvalidIndexException {
		if (index < 0)
			throw new InvalidIndexException(index);

		return new Object();
	}

	public Object firstObject() throws EmptyListException {
		if (length == 0)
			throw new EmptyListException();

		return new Object();
	}

	public int indexOf(Object o) throws ObjectNotFoundException {
		if (o == null)
			throw new ObjectNotFoundException();
		
		return 1;
	}
}
