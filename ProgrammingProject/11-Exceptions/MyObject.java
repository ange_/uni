public class MyObject {
	public static int add(int a, int b) throws MyEx {
		if (a + b == 13)
			throw new MyEx();
		return a + b;
	}


	public static void main(String[] args) {
		try {
			System.out.println(add(5, 8));
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
}
