public class TryWithResourcesTest {
	public static void method(int divisor) {
		try {
			System.out.println("Performing 5/" + divisor);
			int c = 5/divisor;
			return;
		} catch (Exception e) {
			System.err.println("Exception caught");
		} finally {
			System.out.println("Finally-block reached");
		}
	}

	public static void main(String[] args) {
		method(0);
		System.out.println();
		method(1);
	}
}
