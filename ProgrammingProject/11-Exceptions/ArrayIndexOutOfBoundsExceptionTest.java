import java.lang.ArrayIndexOutOfBoundsException;

public class ArrayIndexOutOfBoundsExceptionTest {
	public static void main(String[] args) {
		int[] a = new int[3];
		for (int i = 0; i < a.length; ++i)
			a[i] = i + 1;

		try {
			System.out.println(a[a.length]);
		} catch (ArrayIndexOutOfBoundsException ex) {
			System.out.println(ex.getMessage());
		}

	}
}
