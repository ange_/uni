import java.lang.AutoCloseable;

public class Resource1 implements AutoCloseable {
	@Override
	public void close() {
		System.out.println("Object closed.");
	}

	public static void main(String[] args) {
		try (Resource1 r = new Resource1()) {
			System.out.println("Object created");
			throw new Exception("Exception created");
		} catch (Exception ex) {
			System.err.println(ex.getMessage());
		} finally {
			System.out.println("Finally");
		}
	}
}
