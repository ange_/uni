package org.unibz.prpr1819.util;

import org.junit.Test;

import java.util.ArrayList;

public class AnalyticsPairTest {
	@Test
	public void testEquals() {
		ArrayList<AnalyticsPair> list = new ArrayList<>();
		list.add(new AnalyticsPair(5, "Hello, World"));
		list.add(new AnalyticsPair(93, "asdf"));
		list.add(new AnalyticsPair(6, "word"));
		list.add(new AnalyticsPair(1, "first"));

		if (list.contains(new AnalyticsPair(0, "word")))
			System.out.println("OK");
		else
			System.out.println("ERR");
	}
}
