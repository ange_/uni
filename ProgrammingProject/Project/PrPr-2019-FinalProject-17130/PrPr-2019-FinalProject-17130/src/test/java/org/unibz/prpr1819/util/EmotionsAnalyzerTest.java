package org.unibz.prpr1819.util;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EmotionsAnalyzerTest {
    @Test
    public void testAnalyze() {
    	final String matchpath = "src/test/Resources/emotionWords.csv";
    	SongLyricsAnalyzerAdapter analyzer = null;
        try {
            EmotionsAnalyzer ea = new EmotionsAnalyzer(matchpath);
            analyzer = new SongLyricsAnalyzerAdapter(new SongPair("Bob Dylan", "A Hard Rain's A Gonna Fall"), "src/test/Resources/outdir/", ea.getFileExtension());
            /* It has already been downloaded before and is emotionsTest.csv */
            ea.analyze(analyzer);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        final String comp = analyzer.getOutputCSVFilename();
        final String created = "src/test/Resources/emotionsTest.csv";
        try (BufferedReader r1 = new BufferedReader(new FileReader(comp));
        		BufferedReader r2 = new BufferedReader(new FileReader(created))) {
        	List<String> r1lines = new ArrayList<>();
        	List<String> r2lines = new ArrayList<>();
        	r1.lines().forEach(r1lines::add);
        	r2.lines().forEach(r2lines::add);
        	
        	if (r1lines.size() == r2lines.size())
        		for (int i = 0; i < r1lines.size(); ++i)
        			assertEquals(r1lines.get(i), r2lines.get(i));
        } catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}
