package org.unibz.prpr1819.net;

import org.junit.Before;
import org.junit.Test;
import org.unibz.prpr1819.io.AZLyricsParser;
import org.unibz.prpr1819.io.SongCSVIO;
import org.unibz.prpr1819.util.SongPair;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class LyricsDownloaderTest {
    LyricsDownloader ld = new LyricsDownloader("src/test/Resources/outdir/", new AZLyricsParser());

    @Test
    @Before
    public void testDownload() {
    	SongCSVIO manager = new SongCSVIO();
        try {
            manager.parseFromFile("src/test/Resources/songs.csv");
        } catch (IOException e) {
            System.err.println("IOException...");
            try {
    			manager.close();
    		} catch (IOException ex) {
    			ex.printStackTrace();
    		}
            return;
        }

        try {
        	List<SongPair> lst = manager.getCsvEntries();
        	for (SongPair p : lst)
        		ld.download(new SongURLAdapter(p));
        } catch (SongNotFoundException e) {
            e.printStackTrace();
        }
        
        try {
			manager.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

    @Test
    public void testParse() {
        try {
            ld.parse();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
