package org.unibz.prpr1819.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class SongPairTest {
	SongPair s = new SongPair("AC/DC", "Highway to Hell");
    @Test
    public void testGetArtist() {
    	assertEquals(s.getArtist(), "AC/DC");
    }

    @Test
    public void testGetSong() {
    	assertEquals(s.getSong(), "Highway to Hell");
    }

    @Test
    public void testToString() {
    	assertEquals(s.toString(), "AC/DC - Highway to Hell");
    }
}
