package org.unibz.prpr1819.io;

import org.junit.Test;
import org.unibz.prpr1819.util.SongPair;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SongCSVIOTest {
    private SongCSVIO manager = new SongCSVIO();

    @Test
    public void testParseFromFile() {
        try {
            manager.parseFromFile("src/test/Resources/songs.csv");
        } catch (IOException e) {
            System.err.println("IOException...");
            return;
        }
       
        List<SongPair> pairs = manager.getCsvEntries();
        List<SongPair> songs = new ArrayList<>();
        songs.add(new SongPair("AC/DC", "Hells Bells"));
        songs.add(new SongPair("Avicii", "Wake Me Up"));
        songs.add(new SongPair("AC/DC", "Highway to Hell"));
        songs.add(new SongPair("Bob Dylan", "A Hard Rain's A Gonna Fall"));

        assertEquals("Error", songs, pairs);
    }
}
