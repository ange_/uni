package org.unibz.prpr1819.util;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.FileNotFoundException;

import org.junit.Test;

public class LyricsAnalyzerFactoryTest {
    @Test
    public void testGetAnalyzer() {
    	try {
			assertNotNull(new LyricsAnalyzerFactory("src/test/Resources/emotionWords.csv").getAnalyzer("emotions"));
			assertNotNull(new LyricsAnalyzerFactory("src/test/Resources/ignoreWords.csv").getAnalyzer("frequent words"));
			assertNull(new LyricsAnalyzerFactory().getAnalyzer("custom"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    }
}
