package org.unibz.prpr1819.net;

import org.junit.Test;
import org.unibz.prpr1819.util.SongPair;

import static org.junit.Assert.assertEquals;

public class SongURLAdapterTest {
	@Test
	public void testGetURL() {
		SongURLAdapter sua = new SongURLAdapter(new SongPair("AC/DC", "Hells Bells"));
		System.out.println(sua.getURL());
		assertEquals("https://www.azlyrics.com/lyrics/acdc/hellsbells.html", sua.getURL());
	}

	@Test
	public void testGetFilename() {
		SongURLAdapter sua = new SongURLAdapter(new SongPair("AC/DC", "Hells Bells"));
		System.out.println(sua.getDestHtmlFilename());
		assertEquals("AcDc_HellsBells.html", sua.getDestHtmlFilename());
	}
}
