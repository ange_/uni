package org.unibz.prpr1819;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.unibz.prpr1819.io.SongCSVIOTest;
import org.unibz.prpr1819.net.LyricsDownloaderTest;
import org.unibz.prpr1819.util.AnalyticsPairTest;
import org.unibz.prpr1819.util.EmotionsAnalyzerTest;
import org.unibz.prpr1819.util.FrequentWordsAnalyzerTest;
import org.unibz.prpr1819.util.LyricsAnalyzerFactoryTest;
import org.unibz.prpr1819.util.SongPairTest;

public class TestRunner {
	public static void main(String[] args) {
		System.out.println("SongCSVIO");
		Result res = JUnitCore.runClasses(SongCSVIOTest.class);
		for (Failure f : res.getFailures())
			System.err.println(f.getTestHeader() + ": " + f.getMessage());

		System.out.println(res.wasSuccessful());
		
		System.out.println("Downloader");
		res = JUnitCore.runClasses(LyricsDownloaderTest.class);
		for (Failure f : res.getFailures())
			System.err.println(f.getTestHeader() + ": " + f.getMessage());

		System.out.println(res.wasSuccessful());
		
		System.out.println("AnalyticsPair");
		res = JUnitCore.runClasses(AnalyticsPairTest.class);
		for (Failure f : res.getFailures())
			System.err.println(f.getTestHeader() + ": " + f.getMessage());

		System.out.println(res.wasSuccessful());
		
		System.out.println("EmotionsAnalyzer");
		res = JUnitCore.runClasses(EmotionsAnalyzerTest.class);
		for (Failure f : res.getFailures())
			System.err.println(f.getTestHeader() + ": " + f.getMessage());

		System.out.println(res.wasSuccessful());
		
		System.out.println("FrequentWordAnalyzer");
		res = JUnitCore.runClasses(FrequentWordsAnalyzerTest.class);
		for (Failure f : res.getFailures())
			System.err.println(f.getTestHeader() + ": " + f.getMessage());

		System.out.println(res.wasSuccessful());
		
		System.out.println("LyricsAnalyzerFactory");
		res = JUnitCore.runClasses(LyricsAnalyzerFactoryTest.class);
		for (Failure f : res.getFailures())
			System.err.println(f.getTestHeader() + ": " + f.getMessage());

		System.out.println(res.wasSuccessful());
		
		System.out.println("SongPair");
		res = JUnitCore.runClasses(SongPairTest.class);
		for (Failure f : res.getFailures())
			System.err.println(f.getTestHeader() + ": " + f.getMessage());

		System.out.println(res.wasSuccessful());
	}
}