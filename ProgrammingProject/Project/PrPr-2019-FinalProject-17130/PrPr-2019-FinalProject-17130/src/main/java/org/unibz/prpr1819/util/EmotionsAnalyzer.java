package org.unibz.prpr1819.util;

import org.unibz.prpr1819.util.log.LoggerBuilder;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * EmotionsAnalyzer is an AbstractLyricsAnalzyer specialized in analyzing
 * emotions inside the lyrics.
 */
public class EmotionsAnalyzer extends AbstractLyricsAnalyzer {
	private Logger logger = new LoggerBuilder().getLogger(EmotionsAnalyzer.class.getName());

	/**
	 * Constructs an EmotionsAnalyzer object, by searching the "emotionWords.csv" {@link #matchFile} from
	 * project's Resource folder.
	 * @throws FileNotFoundException If "emotionWords.csv" is not found.
	 */
//	public EmotionsAnalyzer() throws FileNotFoundException {
//		super(EmotionsAnalyzer.class.getClassLoader().getResource("emotionWords.csv").getFile());
//	}

	/**
	 * Constructs an EmotionsAnalyzer object, specifying the {@link #matchFile}.
	 * @param matchFile specified matchFile.
	 * @throws FileNotFoundException If specified matchFile does not exist.
	 */
	public EmotionsAnalyzer(String matchFile) throws FileNotFoundException {
		super(matchFile);
	}

	@Override
	public String getFileExtension() {
		return ".emotioncount.csv";
	}

	/**
	 * Parses the lyrics file (specified by {@link Analyzable}) and counts the number
	 * of emotion words specified in {@link #matchFile}.
	 * After that, a CSV file containing the analytics data is written.
	 * @param song song to analyze.
	 * @throws IOException
	 */
	@Override
	public void analyze(Analyzable song) throws IOException {
		ArrayList<AnalyticsPair> parsedEmotions = new ArrayList<>();

		try (BufferedReader reader = new BufferedReader(new FileReader(song.getClearedLyricsFilename()));
			 BufferedReader emotionsfile = new BufferedReader(new FileReader(matchFile))
		) {
			logger.info("Retrieving emotions from list " + matchFile);
			ArrayList<String> emotionsList = new ArrayList<>();
			emotionsfile.lines().forEach(emotionsList::add);
			emotionsfile.close();
			logger.finer("Emotions retrieved.");

			logger.info("Parsing emotions");
			String buffer = null;
			while ((buffer = reader.readLine()) != null) {
				buffer = buffer.replaceAll("[^0-9a-zA-Z']", " ").replaceAll("\\s+", " ");
				String[] words = buffer.split(" ");

				for (int i = 0; i < words.length; ++i)
					if (emotionsList.contains(words[i])) {
						AnalyticsPair emotion = new AnalyticsPair(1, words[i]);

						if (parsedEmotions.contains(emotion))
							parsedEmotions.get(parsedEmotions.indexOf(emotion)).incrementCounter();
						else
							parsedEmotions.add(emotion);
					}
			}

			logger.finer("Emotions parsed");
		}

		logger.info("Writing emotions on file");
		try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(song.getOutputCSVFilename())))) {
			for (AnalyticsPair emotion : parsedEmotions)
				writer.println(emotion);
		}
	}
}
