package org.unibz.prpr1819;

import org.unibz.prpr1819.util.SongPair;

/**
 * @author ange
 *	Interface defining Song's basic methods.
 *  Songs contain the Song name and the Artist.
 */
public interface BasicSong {
	/**
	 * Returns the song's artist name.
	 * @return the String representing the artist's name
	 */
	String getArtist();

	/**
	 * Returns the song's name.
	 * @return the String representing the song's name
	 */
	String getSong();

	/**
	 * Returns a Pair describing the Artist's name and the Song's name.
	 * @return the Pair containing Artist and Song.
	 */
	SongPair getSongPair();
}
