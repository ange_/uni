package org.unibz.prpr1819.util;

import org.unibz.prpr1819.BasicSong;

/**
 * A {@link Pair} implementation to represent a Song. Also by implementing
 * {@link BasicSong}, SongPair is actually a Song.
 */
public class SongPair extends Pair<String, String> implements BasicSong {

	/**
	 * Creates a new SongPair from an artist and a song.
	 * @param artist String representing the artist
	 * @param song String representing the song name.
	 */
	public SongPair(String artist, String song) {
		super(artist, song);
	}

	/**
	 * Creates a new SongPair from an already existing SongPair
	 * @param sp already existing SongPair.
	 */
	public SongPair(SongPair sp) {
		super(sp);
	}

	/**
	 * Returns the artist of the song (which, for the {@link Pair} is {@link Pair#firstValue}).
	 * @return artist's name.
	 */
	public String getArtist() {
		return firstValue;
	}


	/**
	 * Returns the song name (which for the {@link Pair} is {@link Pair#secondValue}).
	 * @return
	 */
	public String getSong() {
		return secondValue;
	}


	/**
	 * Returns a clone of itself.
	 * @return clone of this SongPair.
	 */
	@Override
	public SongPair getSongPair() {
		return new SongPair(this);
	}


	/**
	 * SongPair's String representation will be ARTIST - SONGNAME.
	 * @return String representation of this SongPair.
	 */
	@Override
	public String toString() {
		return getArtist() + " - " + getSong();
	}
}
