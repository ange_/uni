package org.unibz.prpr1819.io;

import org.unibz.prpr1819.util.Pair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * AbstractCSVIO is an abstract class to perform I/O operations with/on CSV files.
 * Using Generics, internally manages the Pairs using a List of {@link org.unibz.prpr1819.util.Pair}.
 * @param <T> Pair representing CSVs.
 */

abstract public class AbstractCSVIO<T extends Pair> {
	/**
	 * The list containing every {@link org.unibz.prpr1819.util.Pair} parsed from {@link #parseFromFile(String)}.
	 */
	protected List<T> csvEntries = new ArrayList<>();

	/**
	 * Using a specified input CSV file, reads the data and puts it into the List of {@link org.unibz.prpr1819.util.Pair}.
	 * @param inputFile the path of the CSV file to parse.
	 * @throws IOException if an I/O exception occurs.
	 */
	abstract public void parseFromFile(String inputFile) throws IOException;

	/**
	 * Writes the specified analytics {@link org.unibz.prpr1819.util.Pair}.
	 * @param analytics The {@link org.unibz.prpr1819.util.Pair} describing a couple of data to write into the CSV file.
	 * @param outputFile the path of the output CSV file.
	 * @throws IOException if an I/O exception occurs.
	 */
	abstract public void writeToFile(Pair analytics, String outputFile) throws IOException;

	/**
	 * Every entry parsed using {@link #parseFromFile(String)}.
	 * @return
	 */
	public List<T> getCsvEntries() {
		return csvEntries;
	}
}
