package org.unibz.prpr1819.util.log;

import java.io.IOException;
import java.util.logging.*;

/**
 * LoggerBuilder to easily create instances of {@link Logger}.
 */
public class LoggerBuilder {
	/**
	 * Instance of {@link Logger}.
	 */
	private Logger logger;

	/**
	 * Blank constructor.
	 */
	public LoggerBuilder() {

	}


	/**
	 * Creates, sets, assigns and returns an instance of {@link Logger} using a custom configuration.
	 * @param loggerName name of the logger (usually the name of the class where it is created).
	 * @return the new {@link Logger} instance.
	 */
	public Logger getLogger(String loggerName) {
		logger = Logger.getLogger(loggerName);
		logger.setLevel(Level.INFO);
		ConsoleHandler ch = new ConsoleHandler();
		ch.setFormatter(new Formatter() {
			@Override
			public String format(LogRecord record) {
				return String.format("[ %s ] %s: %s\n", record.getLevel(), record.getLoggerName(), record.getMessage());
			}
		});

		logger.addHandler(ch);
		try {
			logger.addHandler(new FileHandler(loggerName + ".log"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return logger;
	}
}
