package org.unibz.prpr1819;

public class Runner {
	public static void main(String[] args) {
		LyricsFacade facade = new LyricsFacade("src/main/Resources/", "src/main/Resources/outfiles/");
		facade.fetchSongs();
		facade.downloadLyrics();
		facade.analyzeLyrics();
	}
}
