package org.unibz.prpr1819.util;

import org.unibz.prpr1819.net.Downloadable;

/**
 * An Analyzable defines that an Analyzer can analyze that object by
 * implementing this interface.
 */
public interface Analyzable extends Downloadable {
	/**
	 * Get resource's cleared lyrics filename.
	 * @return cleared lyrics filename.
	 */
	String getClearedLyricsFilename();

	/**
	 * Get output CSV filename from resource's HTML filename.
	 * @return output CSV filename.
	 */
	String getOutputCSVFilename();
}
