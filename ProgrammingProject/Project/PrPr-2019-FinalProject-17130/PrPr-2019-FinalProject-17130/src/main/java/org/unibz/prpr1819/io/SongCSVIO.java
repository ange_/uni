package org.unibz.prpr1819.io;

import org.unibz.prpr1819.util.Pair;
import org.unibz.prpr1819.util.SongPair;
import org.unibz.prpr1819.util.log.LoggerBuilder;

import java.io.*;
import java.util.logging.Logger;


/**
 * SongCSVIO is a class extending the {@link AbstractCSVIO} class, using {@link SongPair}
 * as a {@link Pair}.
 * To implement {@link SongCSVIO} using a try-with-resources block, the {@link Closeable}
 * interface has been implemented.
 */
public class SongCSVIO extends AbstractCSVIO<SongPair> implements Closeable {
	/**
	 * reader is the stream used from {@link #parseFromFile(String)} to read and parse the file.
	 */
	private BufferedReader reader = null;

	/**
	 * writer is the stream used from {@link #writeToFile(Pair, String)} to write the analytics to a CSV file.
	 */
	private PrintWriter writer = null;

	/**
	 * A logger used to log what happens during the execution.
	 */
	private Logger logger = new LoggerBuilder().getLogger(SongCSVIO.class.getName());

	@Override
	public void parseFromFile(String inputFile) throws IOException {
		reader = new BufferedReader(new FileReader(inputFile));
		String buffer, delimiter = ";";

		logger.info("Reading CSV file: " + inputFile);

		while ((buffer = reader.readLine()) != null) {
			String[] data = buffer.split(delimiter);
			this.csvEntries.add(new SongPair(data[1], data[0]));
		}
	}

	@Override
	public void writeToFile(Pair analytics, String outputFile) throws IOException {
		logger.info("Writing CSV file: " + outputFile);
		writer = new PrintWriter(new BufferedWriter(new FileWriter(outputFile)));
		writer.println(analytics.getFirstValue() + ";" + analytics.getSecondValue());
	}

	/**
	 * Implemented from {@link Closeable#close()}, allows using try-with-resources
	 * block and guarantees that the streams can/will be closed.
	 * @overrides {@link Closeable#close()}
	 * @throws IOException If an I/O exception occurs.
	 */
	@Override
	public void close() throws IOException {
		if (reader != null) {
			reader.close();
			logger.info("Reader closed.");
		}

		if (writer != null) {
			writer.close();
			logger.info("Writer closed.");
		}
	}
}
