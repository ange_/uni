package org.unibz.prpr1819.net;

/**
 * A Downloadable defines that the object implementing this interface
 * can be used to download a file.
 */
public interface Downloadable {
	/**
	 * Calculates and returns the URL of the resource to download.
	 * @return the URL of the resource.
	 */
	String getURL();

	/**
	 * Calculates and returns the to-download/downloaded HTML filename.
	 * @return the path of the downloaded resource.
	 */
	String getDestHtmlFilename();
}
