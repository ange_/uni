package org.unibz.prpr1819.net;

import org.unibz.prpr1819.util.SongPair;

/**
 * SongURLAdapter is an adapter used to convert a {@link SongPair} into a {@link Downloadable}
 * object, with relative methods used to manage download operations.
 */
public class SongURLAdapter extends SongPair implements Downloadable {
	/**
	 * Lyrics' default engine.
	 */
	public final static String baseurl = "https://www.azlyrics.com";

	private String destHtmlFilename = null;

	/**
	 * Constructs SongURLAdapter using {@link SongPair}, adapting it for download operations.
	 * @param song the song to adapt.
	 */
	public SongURLAdapter(SongPair song) {
		super(song);
	}

	public String getURL() {
		String artist_url = this.getArtist().toLowerCase().replaceAll("[^0-9a-zA-Z]", "");
		String song_url = this.getSong().toLowerCase().replaceAll("[^0-9a-zA-Z]", "");
		return String.format("%s/lyrics/%s/%s.html", baseurl, artist_url, song_url);
	}

	public String getDestHtmlFilename() {
		if (this.destHtmlFilename != null)
			return this.destHtmlFilename;

		String[] artist_tokens = getArtist().replaceAll("[^0-9a-zA-Z]", " ").split(" ");
		String[] song_tokens = getSong().replaceAll("[^0-9a-zA-Z]", " ").split(" ");

		StringBuilder filename = new StringBuilder();
		for (int i = 0; i < artist_tokens.length; ++i)
			filename.append(Character.toUpperCase(artist_tokens[i].charAt(0)) + artist_tokens[i].substring(1).toLowerCase());

		filename.append('_');

		for (int i = 0; i < song_tokens.length; ++i)
			filename.append(Character.toUpperCase(song_tokens[i].charAt(0)) + song_tokens[i].substring(1).toLowerCase());

		this.destHtmlFilename = filename.append(".html").toString();  // Saving into a variable, avoids overhead.
		return this.destHtmlFilename;
	}

	/**
	 * Converting this object into a String object results into obtaining resource's URL.
	 * @return {@link #getURL()}
	 */
	@Override
	public String toString() {
		return getURL();
	}
}
