package org.unibz.prpr1819.util;

import java.io.FileNotFoundException;

/**
 * LyricsAnalyzerFactory is a Factory whose purpose is facilitate the creation
 * of LyricsAnalyzer objects.
 */
public class LyricsAnalyzerFactory {
	/**
	 * matchFile containing the words to include-exclude during analysis.
	 */
	private String matchFile;


	/**
	 * Creates a new LyricsAnalyzerFactory specifying the {@link #matchFile}.
	 * @param matchFile
	 */
	public LyricsAnalyzerFactory(String matchFile) {
		this.matchFile = matchFile;
	}

	/**
	 * Creates a new blank LyricsAnalyzerFactory ({@link #matchFile} is null).
	 */
	public LyricsAnalyzerFactory() {
		matchFile = null;
	}

	/**
	 * Sets a value to {@link #matchFile}.
	 * @param matchFile value for {@link #matchFile}.
	 */
	public void setMatchFile(String matchFile) {
		this.matchFile = matchFile;
	}

	/**
	 * Returns a new Analyzer depending on the type specified as input.
	 * It can be "emotions" or "frequent words".
	 * @param type String specifying which kind of analyzer you want.
	 * @return specialized LyricsAnalyzer or null.
	 * @throws FileNotFoundException If {@link #matchFile} is not found.
	 */
	public AbstractLyricsAnalyzer getAnalyzer(String type) throws FileNotFoundException {
		switch(type.toLowerCase()) {
			case "emotions":
				return new EmotionsAnalyzer(this.matchFile);

			case "frequent words":
				return new FrequentWordsAnalyzer(this.matchFile);
		}

		return null;
	}
}
