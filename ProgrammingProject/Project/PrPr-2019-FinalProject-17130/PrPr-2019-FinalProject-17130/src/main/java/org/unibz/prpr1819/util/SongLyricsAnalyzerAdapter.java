package org.unibz.prpr1819.util;

import org.unibz.prpr1819.net.LyricsDownloader;
import org.unibz.prpr1819.net.SongURLAdapter;

/**
 * SongLyricsAnalyzerAdapter transforms a SongPair ({@link SongURLAdapter} extends it)
 * into an {@link Analyzable} object.
 */
public class SongLyricsAnalyzerAdapter extends SongURLAdapter implements Analyzable {
	/**
	 * Output directory that has been used.
	 */
	private String downloadOutputDir;

	/**
	 * CSV file's extension that will be used by the {@link AbstractLyricsAnalyzer}.
	 */
	private String csvExtension;


	/**
	 * Creates a new SongLyricsAnalyzerAdapter from a {@link SongPair} and {@link #downloadOutputDir}.
	 * This constructor sets {@link #csvExtension} to "".
	 * @param song to analyze
	 * @param downloadOutputDir download directory that has been used.
	 */
	public SongLyricsAnalyzerAdapter(SongPair song, String downloadOutputDir) {
		super(song);
		this.downloadOutputDir = downloadOutputDir;
		this.csvExtension = "";
	}


	/**
	 * Creates a new SongLyricsAnalyzerAdapter from a {@link SongPair} and {@link #downloadOutputDir}
	 * and {@link #csvExtension}.
	 * @param song
	 * @param downloadOutputDir
	 * @param csvExtension
	 */
	public SongLyricsAnalyzerAdapter(SongPair song, String downloadOutputDir, String csvExtension) {
		super(song);
		this.downloadOutputDir = downloadOutputDir;
		this.csvExtension = csvExtension;
	}

	/**
	 * Creates and returns the String representing the Song's HTML file.
	 * @return song's HTML file.
	 */
	@Override
	public String getDestHtmlFilename() {
		return this.downloadOutputDir + super.getDestHtmlFilename();
	}

	/**
	 * Sets {@link #csvExtension}.
	 * @param csvExtension specified csvExtension.
	 */
	public void setCsvExtension(String csvExtension) {
		this.csvExtension = csvExtension;
	}


	/**
	 * Returns the String representing the analyzer's output CSV file.
	 * @return output CSV file.
	 */
	public String getOutputCSVFilename() {
		return getDestHtmlFilename() + csvExtension;
	}


	/**
	 * Returns the path to the cleared lyrics file.
	 * @return output cleared lyrics file.
	 */
	@Override
	public String getClearedLyricsFilename() {
		return this.getDestHtmlFilename() + LyricsDownloader.cleanedLyricsExtension;
	}
}
