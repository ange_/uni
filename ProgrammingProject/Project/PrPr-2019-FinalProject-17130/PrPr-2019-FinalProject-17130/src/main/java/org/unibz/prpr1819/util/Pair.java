package org.unibz.prpr1819.util;

/**
 * Pair class of this package.
 * @param <T> Type of the first value (every type is ok)
 * @param <U> Type of the second value (every type is ok)
 */
abstract public class Pair<T, U> {
	/**
	 * First value of the pair. Can be changed.
	 */
	protected T firstValue;

	/**
	 * Second value of the pair. Can also be implemented as final.
	 */
	protected U secondValue;


	/**
	 * Creates a new Pair using two values.
	 * @param firstValue first value of the pair.
	 * @param secondValue second value of the pair.
	 */
	public Pair(T firstValue, U secondValue) {
		this.firstValue = firstValue;
		this.secondValue = secondValue;
	}


	/**
	 * Creates a new Pair from an existing Pair
	 * @param pair existing pair from which the new one will be created.
	 */
	@SuppressWarnings("unchecked")
	public Pair(Pair pair) {
		this.firstValue = (T)pair.getFirstValue();
		this.secondValue = (U)pair.getSecondValue();
	}


	/**
	 * Returns the first value.
	 * @return {@link #firstValue}
	 */
	public T getFirstValue() {
		return firstValue;
	}


	/**
	 * Returns the second value.
	 * @return {@link #secondValue}
	 */
	public U getSecondValue() {
		return secondValue;
	}

	/**
	 * Overriding equals() to allow Pairs comparisons.
	 * @param obj object to compare
	 * @return true if this pair and obj are equals; false otherwise.
	 */
	@Override
	public boolean equals(Object obj) {
		@SuppressWarnings("unchecked")
		Pair<T, U> o = (Pair<T, U>)obj;

		return this.firstValue.equals(o.firstValue) && this.secondValue.equals(o.secondValue);
	}
}