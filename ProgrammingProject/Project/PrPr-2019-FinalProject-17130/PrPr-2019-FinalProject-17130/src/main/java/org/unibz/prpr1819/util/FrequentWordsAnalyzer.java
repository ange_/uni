package org.unibz.prpr1819.util;

import org.unibz.prpr1819.util.log.LoggerBuilder;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * FrequentWordsAnalyzer is an AbstractLyricsAnalyzer specialized in analyzing
 * the most used words in lyrics and write the 20 most frequent ones.
 */
public class FrequentWordsAnalyzer extends AbstractLyricsAnalyzer {
	private Logger logger = new LoggerBuilder().getLogger(FrequentWordsAnalyzer.class.getName());

	/**
	 * Constructs a FrequentWordsAnalzyer object, by searching the "ignoreWords.csv" {@link #matchFile} from
	 * project's Resource folder.
	 * @throws FileNotFoundException If "ignoreWords.csv" is not found.
	 */
	public FrequentWordsAnalyzer() throws FileNotFoundException {
		/* Thx to: https://www.mkyong.com/java/java-read-a-file-from-resources-folder/ */
		super(FrequentWordsAnalyzer.class.getClassLoader().getResource("ignoreWords.csv").getFile());
	}

	/**
	 * Constructs an FrequentWordsAnalyzer object, specifying the {@link #matchFile}.
	 * @param matchFile specified matchFile.
	 * @throws FileNotFoundException If specified matchFile does not exist.
	 */
	public FrequentWordsAnalyzer(String matchFile) throws FileNotFoundException {
		super(matchFile);
	}

	@Override
	public String getFileExtension() {
		return ".wordcount.csv";
	}

	/**
	 * Parses the lyrics file (specified by {@link Analyzable}) and counts the number
	 * of words inside the lyrics file.
	 * After that, a CSV file containing the 20-most-used ones is written.
	 * @param song song to analyze.
	 * @throws IOException
	 */
	@Override
	public void analyze(Analyzable song) throws IOException {
		ArrayList<AnalyticsPair> analyzed_words = null;

		try (BufferedReader reader = new BufferedReader(new FileReader(song.getClearedLyricsFilename()));
			 BufferedReader ignoredWordsFile = new BufferedReader(new FileReader(this.matchFile))
		) {
			ArrayList<String> ignoredWords = new ArrayList<>();
			ignoredWordsFile.lines().forEach(ignoredWords::add);
			ignoredWordsFile.close();
			logger.info("Ignored words fetched.");

			String buffer = null;
			analyzed_words = new ArrayList<>();

			logger.finer("Analyzing frequent words.");
			while ((buffer = reader.readLine()) != null) {
				buffer = buffer.replaceAll("[^0-9a-zA-Z']", " ").replaceAll("\\s+", " ");
				String[] words = buffer.split(" ");

				for (int i = 0; i < words.length; ++i) {
					if (words[i].isEmpty())
						continue;

					if (! ignoredWords.contains(words[i])) {
						AnalyticsPair current_word = new AnalyticsPair(1, words[i]);

						if (analyzed_words.contains(current_word))
							analyzed_words.get(analyzed_words.indexOf(current_word)).incrementCounter();
						else
							analyzed_words.add(current_word);
					}
				}
			}

		} catch (FileNotFoundException e) {
			logger.severe(e.getMessage());
			e.printStackTrace();
		}

		analyzed_words.sort((a, b) -> b.compareTo(a));
		logger.info("Writing 20 frequent words on " + song.getOutputCSVFilename());
		try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(song.getOutputCSVFilename())))) {
			for (int i = 0; i < 20; i++)
				writer.println(analyzed_words.get(i));
		}
	}
}
