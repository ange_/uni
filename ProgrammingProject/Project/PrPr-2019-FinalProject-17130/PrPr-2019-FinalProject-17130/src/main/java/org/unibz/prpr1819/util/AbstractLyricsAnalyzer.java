package org.unibz.prpr1819.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * AbstractLyricsAnalyzer is an abstract class defining how a basic
 * lyrics analyzer behaves.
 */
abstract public class AbstractLyricsAnalyzer {
	/**
	 * matchFile is the file that will be used to perform checks (to include or exclude words
	 * from the analyze).
	 */
	protected final String matchFile;

	/**
	 * Creates an {@link AbstractLyricsAnalyzer} with {@link #matchFile}.
	 * @param matchFile the file used to perform checks.
	 * @throws FileNotFoundException If {@link #matchFile} does not exist.
	 */
	public AbstractLyricsAnalyzer(String matchFile) throws FileNotFoundException {
		this.matchFile = matchFile;

		if (! new File(this.matchFile).exists())
			throw new FileNotFoundException(this.matchFile + " does not exist");
	}

	/**
	 * The file extension of the results of the lyrics' analysis
	 * is analyzer-specific and returned by this method.
	 * @return the file extension that the output analyzed file has.
	 */
	abstract public String getFileExtension();

	/**
	 * Analyzes an {@link Analyzable} song using {@link #matchFile}.
	 * @param song song to analyze.
	 * @throws IOException If I/O problems occurs during execution.
	 */
	abstract public void analyze(Analyzable song) throws IOException;
}
