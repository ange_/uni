package org.unibz.prpr1819.util;

/**
 * A {@link Pair} implementation for Analytics data.
 * Is implemented by extending {@link Pair} with Integer-String specialization
 * and implementing {@link Comparable}.
 */
public class AnalyticsPair extends Pair<Integer, String> implements Comparable<AnalyticsPair> {
	/**
	 * Constructs AnalyticsPair using an Integer and a String.
	 * @param counter number of times a word appears/has appeard.
	 * @param word word to analyze.
	 */
	public AnalyticsPair(int counter, String word) {
		super(counter, word);
	}

	/**
	 * In the pair, the first value represents how many times the word appears.
	 * It is represented by counter: this counter can be returned by getCounter().
	 * @return number of times the word appears.
	 */
	public Integer getCounter() {
		return firstValue;
	}

	/**
	 * In the pair, the second value represents the word to analyze and is
	 * returned by getWord() itself.
	 * @return word to analyze
	 */
	public String getWord() {
		return secondValue;
	}

	/**
	 * Increments how many times the word appears (by one).
	 */
	public void incrementCounter() {
		++firstValue;
	}

	/**
	 * Checks for the equality between an Object and an AnalyticsPair.
	 * @param obj object to check if equal.
	 * @return true if (case-insensitive) word is equal to obj.
	 */
	@Override
	public boolean equals(Object obj) {
		return ((AnalyticsPair)obj).getWord().equalsIgnoreCase(this.getWord());
	}

	/**
	 *
	 * @return the CSV representation of the AnalyticsPair.
	 */
	@Override
	public String toString() {
		return getCounter() + ";" + getWord();
	}

	/**
	 * @param o AnalyticsPair to compare
	 * @return result of Integer.compare({@link #getCounter()}, o.getCounter())
	 */
	@Override
	public int compareTo(AnalyticsPair o) {
		return Integer.compare(getCounter(), o.getCounter());
	}
}
