package org.unibz.prpr1819;

import org.unibz.prpr1819.io.AZLyricsParser;
import org.unibz.prpr1819.io.SongCSVIO;
import org.unibz.prpr1819.net.LyricsDownloader;
import org.unibz.prpr1819.net.SongNotFoundException;
import org.unibz.prpr1819.net.SongURLAdapter;
import org.unibz.prpr1819.util.*;
import org.unibz.prpr1819.util.log.LoggerBuilder;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * LyricsFacade simplifies the use of the infrastructure created to fetch,
 * download and analyze the lyrics of the specified songs.
 */
public class LyricsFacade {
	/**
	 * Filepath where songs are fetched.
	 */
	private final String songsCsvFilepath;

	/**
	 * Output directory for downloaded data.
	 */
	protected final String downloadOutputDir;

	/**
	 * Songs fetched from {@link #songsCsvFilepath}.
	 */
	private List<SongPair> parsedSongs;

	/**
	 * Logger to track what happens during execution.
	 */
	private Logger logger = new LoggerBuilder().getLogger(LyricsFacade.class.getName());
	
	/**
	 * Resources path
	 */
	protected String resources;


	/**
	 * Creates a new LyricsFacade using {@link #songsCsvFilepath} and {@link #downloadOutputDir}.
	 * @param songsCsvFilepath CSV file containing the list of songs to download and analyze.
	 * @param downloadOutputDir output directory for downloaded data.
	 */
	public LyricsFacade(String resources, String downloadOutputDir) {
		this.resources = resources;
		this.songsCsvFilepath = this.resources + "songs.csv";
		this.downloadOutputDir = downloadOutputDir;
	}

	/**
	 * Method to fetch songs from {@link #songsCsvFilepath} and store them in {@link #parsedSongs}.
	 */
	public void fetchSongs() {
		SongCSVIO songsFetcher = new SongCSVIO();

		try {
			logger.info("Fetching data from CSV file \"" + songsCsvFilepath + "\"");
			songsFetcher.parseFromFile(songsCsvFilepath);
		} catch (IOException e) {
			e.printStackTrace();
		}

		logger.fine("Data fetched");
		parsedSongs = songsFetcher.getCsvEntries();
		
		try {
			songsFetcher.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * Download lyrics for each songs using multithreading.
	 */
	public void downloadLyrics() {
		/* Before reading https://stackoverflow.com/a/7502815 solution, the used solution
		 * was creating a private class for LyricsFacade that extended Thread and managed
		 * the download and parsing for each single song.
		 * An ArrayList of that class would be used and created.
		 * But thanks to that answer, an inline-version of my solution
		 * has been used (which makes the code much cleaner).
		 */
		List<Thread> threads = new ArrayList<>();

		for (SongPair song : parsedSongs) {
			logger.fine("Creating and executing download Thread list");
			SongURLAdapter adapter = new SongURLAdapter(song);
			LyricsDownloader ld = new LyricsDownloader(this.downloadOutputDir, new AZLyricsParser());

			Thread t = new Thread() {
				@Override
				public void run() {
					try {
						logger.info("Downloading: " + song);
						ld.download(adapter);
					} catch (SongNotFoundException e) {
						logger.severe("Song " + song + " does not exist.");
						parsedSongs.remove(song);
						return;
					}

					try {
						logger.info("Parsing: " + song);
						ld.parse();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}

					logger.fine(song + " finished.");
				}
			};
			t.start();
			threads.add(t);
		}

		for (Thread t : threads) {
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Analyze each downloaded song, using multithreading.
	 */
	public void analyzeLyrics() {
		List<Thread> threads = new ArrayList<>();

		for (SongPair song : parsedSongs) {
			logger.fine("Creating and executing download Thread list");
			Thread t = new Thread(new AnalyzerThread(new SongLyricsAnalyzerAdapter(song, this.downloadOutputDir), this.resources));
			t.start();
			threads.add(t);
		}

		for (Thread t : threads) {
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * AnalyzerThread is a private class implementing {@link Runnable} to implement threads
	 * to analyze lyrics data.
	 */
	private class AnalyzerThread implements Runnable {
		/**
		 * Adapter to convert a SongPair into a format readable for Analyzers.
		 */
		private SongLyricsAnalyzerAdapter adapter;

		
		private String resources;
		/**
		 * Creates a new AnalyzerThread assigning {@link #adapter}.
		 * @param adapter
		 */
		public AnalyzerThread(SongLyricsAnalyzerAdapter adapter, String resources) {
			this.adapter = adapter;
			this.resources = resources;
		}

		@Override
		public void run() {
			AbstractLyricsAnalyzer ea = null, fwa = null;
			try {
				logger.finer("Creating analyzers");
				ea = new LyricsAnalyzerFactory(this.resources + "emotionWords.csv").getAnalyzer("emotions");
				fwa = new LyricsAnalyzerFactory(this.resources + "ignoreWords.csv").getAnalyzer("frequent words");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			try {
				logger.info("Analyzing: " + adapter.getDestHtmlFilename());
				adapter.setCsvExtension(ea.getFileExtension());
				ea.analyze(adapter);

				adapter.setCsvExtension(fwa.getFileExtension());
				fwa.analyze(adapter);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
