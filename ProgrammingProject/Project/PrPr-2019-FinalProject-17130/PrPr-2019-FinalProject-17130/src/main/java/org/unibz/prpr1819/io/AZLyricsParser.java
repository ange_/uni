package org.unibz.prpr1819.io;

import java.io.*;

/**
 * AZLyricsParser is a parser implementing {@link LyricsParser} using azlyrics.com.
 * Since no external libraries are allowed, JSoup cannot be used.
 * XML parsers do not work, since HTML allows not-ending tags (e.g. <br>),
 * so it would result in a runtime error. Knowning the source, a workaround
 * is parsing the expected result.
 */
public class AZLyricsParser implements LyricsParser {

	@Override
	public void parse(String htmlFile, String cleanMdFile) throws FileNotFoundException {
		try (BufferedReader reader = new BufferedReader(new FileReader(htmlFile));
			 PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(cleanMdFile)))
		) {
			String buffer = null;

			boolean divringtone_found = false;
			int textdiv_idx = -1, textdiv_endidx = -1;
			while ((buffer = reader.readLine()) != null) {
				buffer = buffer.replaceAll("<br>", "\n");
				buffer = buffer.replaceAll("&quot;", "\"");

				if (! divringtone_found) {
					int idx = buffer.indexOf("<div class=\"ringtone\">");
					int closing_idx = buffer.indexOf("</div>", idx);
					buffer = buffer.substring(closing_idx + "</div>".length());

					if (idx != -1 && closing_idx != -1)
						divringtone_found = true;
				}

				if (divringtone_found) {
					textdiv_idx = buffer.indexOf("<div>");
				}

				if (textdiv_idx != -1)
					textdiv_endidx = buffer.indexOf("</div>", textdiv_idx);

				if (textdiv_endidx != -1)
					buffer = buffer.substring(textdiv_idx, textdiv_endidx);

				buffer = buffer.replaceAll("<[^>]+>", "");

				writer.write(buffer);
			}

		} catch (FileNotFoundException e) {  // File not found must be managed from the caller
			throw e;  // Must be done manually, since FileNotFoundException is a subclass of IOException.
		} catch (IOException e) {  // IOException is unpredictable, so is not managed.
			e.printStackTrace();
		}
	}
}
