package org.unibz.prpr1819.net;

/**
 * If during the download operation the song is not found on the server,
 * a new SongNotFoundException is thrown.
 */
public class SongNotFoundException extends Exception {
	@Override
	public String getMessage() {
		return "The requested song does not exist.\n" + super.getMessage();
	}
}
