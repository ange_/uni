package org.unibz.prpr1819.io;

import java.io.FileNotFoundException;

/**
 * A LyricsParser defines methods to parse lyrics from a source file
 * and writes them into a cleaned output file.
 */
public interface LyricsParser {
	/**
	 * Parses song lyrics from a source file and writes them back into a cleaned output file.
	 * @param htmlFile the source file to parse.
	 * @param cleanMdFile the output cleaned file.
	 * @throws FileNotFoundException If the specified source file does not exist.
	 */
	void parse(String htmlFile, String cleanMdFile) throws FileNotFoundException;
}
