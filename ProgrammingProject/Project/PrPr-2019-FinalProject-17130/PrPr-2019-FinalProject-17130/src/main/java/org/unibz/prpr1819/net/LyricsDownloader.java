package org.unibz.prpr1819.net;

import org.unibz.prpr1819.io.LyricsParser;
import org.unibz.prpr1819.util.log.LoggerBuilder;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Logger;

/**
 * LyricsDownloader manages and performs download operations using a {@link Downloadable} object.
 */
public class LyricsDownloader {
	private final String outputDir;
	private String destHtmlFilename = null;
	private final LyricsParser lyricsParser;
	private Logger logger = new LoggerBuilder().getLogger(LyricsDownloader.class.getName());

	/**
	 * Specifies the extension that the cleaned file will have.
	 */
	public final static String cleanedLyricsExtension = ".clean.md";

	/**
	 * Constructs a {@link LyricsDownloader} specifying an output directory and the lyrics
	 * parser that will be used.
	 * @param outputDir the directory where the files are going to be downloaded.
	 * @param lyricsParser parser used to parse the downloaded HTML file.
	 */
	public LyricsDownloader(String outputDir, LyricsParser lyricsParser) {
		this.outputDir = outputDir;
		this.lyricsParser = lyricsParser;
	}

	/**
	 * If {@link #download(Downloadable)} has not been called, returns null.
	 * Otherwise returns the downloaded HTML file.
	 * @return the downloaded HTML file.
	 */
	public String getDestHtmlFilename() {
		return destHtmlFilename;
	}

	/**
	 * Using {@link #getDestHtmlFilename()} and {@link #cleanedLyricsExtension}, retuns
	 * the cleaned lyrics filename.
	 * @return cleaned lyrics filename.
	 */
	public String getCleanedLyricsFilename() {
		return getDestHtmlFilename() + cleanedLyricsExtension;
	}

	/**
	 * Using this methods, forces a specified destination HTML filename
	 * (should be used only for testing purposes).
	 * @param destHtmlFilename
	 */
	public void setDestHtmlFilename(String destHtmlFilename) {
		this.destHtmlFilename = destHtmlFilename;
	}

	/**
	 * Downloads the specified resource: if the resource does not exists,
	 * {@link SongNotFoundException} is thrown.
	 * @param d resource to download
	 * @throws SongNotFoundException if the resource does not exist.
	 */
	public void download(Downloadable d) throws SongNotFoundException {
		String songlink = d.getURL();
		setDestHtmlFilename(outputDir + d.getDestHtmlFilename());
		logger.finer("Set songlink and output filename");

		URLConnection uc = null;
		try {
			uc = new URL(songlink).openConnection();
		} catch (MalformedURLException e) {
			logger.severe(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.severe(e.getMessage());
			e.printStackTrace();
		}
		if (! (uc instanceof HttpURLConnection))
			throw new IllegalArgumentException(songlink + " is not an HTTP resource");

		logger.fine(songlink + " was an HTTP resource");
		// Casting uc to HttpURLConnection is redundant, as we know that it is an HTTP resource.
		// Plus, at the moment we do not need particular HttpURLConnection, but just plain download.
		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
		} catch (FileNotFoundException e) {
			throw new SongNotFoundException();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(this.destHtmlFilename)))) {
			String buffer = null;
			while ((buffer = in.readLine()) != null)
				writer.write(buffer);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		logger.info(d.getDestHtmlFilename() + " downloaded");
	}

	/**
	 * Parses the HTML file and writes the lyrics into {@link #getCleanedLyricsFilename()}.
	 * @throws FileNotFoundException If the file does not exist.
	 */
	public void parse() throws FileNotFoundException {
		logger.info("Parsing the retrieved HTML file.");
		lyricsParser.parse(this.destHtmlFilename, getCleanedLyricsFilename());
	}
}
