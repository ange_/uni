public class Processor {
	public int fibonacci(int n) {
		if (n < 0)
			throw new IllegalArgumentException("n >= 0");

		return n < 2 ? 1 : fibonacci(n - 1) + fibonacci(n - 2);
	}
}
