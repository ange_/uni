import org.junit.Test;

public class TestException {

	@Test(expected = IllegalArgumentException.class)
	public void testException() {
		throw new IllegalArgumentException("Here we go");
	}
}
