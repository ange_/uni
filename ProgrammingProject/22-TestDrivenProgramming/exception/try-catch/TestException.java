import static org.junit.Assert.fail;
import org.junit.Test;

public class TestException {
	public void testException() {
		try {
			throw new IllegalArgumentException("Here we go");
			fail("IllegalArgumentException expected...");
		} catch (IllegalArgumentException e) {
			// Ignore, is expected.
		}
	}
}
