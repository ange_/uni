import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

public class Runner {
	public static void main(String[] args) {
		Result res = JUnitCore.runClasses(TestException.class);
		System.out.println(res.wasSuccessful());
	}
}
