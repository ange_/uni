import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;

public class BasicAnnotation {
	@BeforeClass
	public static void runOnceBeforeClass() {
		System.out.println("@BeforeClass: runOnceBeforeClass()");
	}

	@AfterClass
	public static void runOnceAfterClass() {
		System.out.println("@AfterClass: runOnceAfterClass()");
	}

	@Before
	public void runBefore() {
		System.out.println("@Before: runBefore()");
	}

	@After
	public void runAfter() {
		System.out.println("@After: runAfter()");
	}

	@Test
	public void test1() {
		System.out.println("@Test: test1()");
	}

	@Test
	public void test2() {
		System.out.println("@Test: test2()");
	}
}
