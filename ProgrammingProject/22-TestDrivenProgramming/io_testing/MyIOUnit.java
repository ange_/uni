import java.util.List;
import java.util.ArrayList;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.lang.StringBuilder;

public class MyIOUnit {
	protected List<String> tokens = new ArrayList<>();

	public void read(InputStream input) throws IOException {
		StringBuilder builder = new StringBuilder();

		for (int data = input.read(); data >= 0; data = input.read())
			if ((char)data != ',')
				builder.append((char)data);
			else {
				this.tokens.add(builder.toString().trim());
				builder.delete(0, builder.length());
			}
	}

	public void write(OutputStream output) throws IOException {
		for (int i = 0; i < tokens.size(); ++i) {
			if (i != 0)
				output.write(',');
			output.write(tokens.get(i).getBytes());
		}
	}
}
