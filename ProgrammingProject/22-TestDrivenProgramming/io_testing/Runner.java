import org.junit.runner.JUnitCore;

public class Runner {
	public static void main(String[] args) {
		System.out.println(JUnitCore.runClasses(MyIOUnitTest.class).wasSuccessful());
	}
}
