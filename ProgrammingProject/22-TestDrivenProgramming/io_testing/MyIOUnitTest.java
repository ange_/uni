import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public class MyIOUnitTest {
	@Test
	public void testRead() {
		MyIOUnit io = new MyIOUnit();
		byte[] data = "123,456,789,123,456,789".getBytes();

		InputStream input = new ByteArrayInputStream(data);

		try {
			io.read(input);
		} catch (IOException e) { }

		assertEquals("123", io.tokens.get(0));
		assertEquals("456", io.tokens.get(1));
	}

	@Test
	public void testWrite() {
		MyIOUnit io = new MyIOUnit();
		ByteArrayOutputStream output = new ByteArrayOutputStream();

		io.tokens.add("one");
		io.tokens.add("two");
		io.tokens.add("three");

		try {
			io.write(output);
		} catch (IOException e) { }

		String string = new String(output.toByteArray());
		assertEquals("one,two,three", string);
	}
}
