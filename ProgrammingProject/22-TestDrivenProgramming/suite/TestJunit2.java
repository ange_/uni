import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestJunit2 {
	@Test
	public void testSalutationMessage() {
		MessageUtil msg = new MessageUtil("Robert");
		assertEquals("Hi! Robert", msg.salutationMessage());
	}
}
