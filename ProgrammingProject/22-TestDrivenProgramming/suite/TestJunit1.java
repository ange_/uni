import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestJunit1 {
	@Test
	public void testPrintMessage() {
		MessageUtil msg = new MessageUtil("Hello, World!");
		assertEquals("Hello, World!", msg.printMessage());
	}
}
