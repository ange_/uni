import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class Runner {
	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(JUnitTestSuite.class);

		for (Failure fail : result.getFailures())
			System.err.println(fail);

		System.out.println(result.wasSuccessful());
	}
}
