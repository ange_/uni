import static org.junit.Assert.*;
import org.junit.Test;

public class MyTests {
	@Test
	public void MultiplicationByZeroReturnZero() {
		MyClass tester = new MyClass();

		assertEquals("14*0 should return 0", 0, tester.multiply(14, 0));
	}
}
