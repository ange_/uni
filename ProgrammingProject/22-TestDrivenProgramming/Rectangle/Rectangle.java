public class Rectangle {
	private int x = 0, y = x;

	public Rectangle(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getArea() {
		return x * y;
	}

	public boolean equals(Object o) {
		return this.getArea() == ((Rectangle) o).getArea();
	}
}
