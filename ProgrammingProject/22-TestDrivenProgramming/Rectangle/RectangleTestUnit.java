import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class RectangleTestUnit {
	@Test
	public void testEquals() {
		Rectangle r1 = new Rectangle(5, 3);
		Rectangle r2 = new Rectangle(3, 5);

		assertEquals(r1, r2);
	}
}
