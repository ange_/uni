import org.junit.Test;
import static org.junit.Assert.*;

public class MyUnitTest {
	@Test
	public void testConcatenate() {
		MyUnit u = new MyUnit();
		assertEquals("Hello, World!", u.concatenate("Hello, ", "World!"));
	}
}
