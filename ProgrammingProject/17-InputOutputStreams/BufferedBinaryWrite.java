import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.BufferedOutputStream;

public class BufferedBinaryWriter {
	public static void main(String[] args) {
		try (DataOutputStream writer = new DataOutputStream(
					new BufferedOutputStream(new FileOutputStream("buffered_bin-write.dat")))) {
			for (int i = 0; i < 12; ++i)
				writer.writeInt(i);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
