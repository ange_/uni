import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.FileNotFoundException;

public class CopyBinary {
	public static boolean copy(String source, String dest) throws FileNotFoundException {
		try (
			DataInputStream reader = new DataInputStream(new BufferedInputStream(new FileInputStream(source)));
			DataOutputStream writer = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(dest)))
		) {
			int buffer = 0;
			while (true) {
				buffer = reader.readUnsignedByte();
				writer.writeByte(buffer);
			}
		} catch (EOFException e) {
			return true;
		} catch (FileNotFoundException e) {
			throw e;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;
	}
	public static void main(String[] args) {
		if (args.length != 2) {
			System.err.println("Usage: " + CopyBinary.class.getName() + " [source] [dest]");
			System.exit(1);
		}

		try {
			if (! copy(args[0], args[1]))
				System.err.println("Error during copy operation.");
		} catch (FileNotFoundException e) {
			System.err.println(args[0] + " not found.");
		}
	}
}
