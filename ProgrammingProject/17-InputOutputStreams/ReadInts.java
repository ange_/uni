import java.io.FileInputStream;
import java.io.DataInputStream;
import java.io.BufferedInputStream;
import java.io.IOException;

public class ReadInts {
	public static void main(String[] args) {
		int sum = 0;
		try (DataInputStream reader = new DataInputStream(new BufferedInputStream(new FileInputStream("file.dat")))) {
			for (int i = 0; i < 4; ++i)
				sum += reader.readInt();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Result: " + sum);
	}
}
