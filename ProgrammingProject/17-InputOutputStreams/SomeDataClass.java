import java.io.Serializable;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.FileInputStream;

public class SomeDataClass implements Serializable {
	private String name;
	private transient int age;
	private String address;

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public String getAddress() {
		return address;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public SomeDataClass(String name, int age, String address) {
		this.name = name;
		this.age = age;
		this.address = address;
	}

	public String toString() {
		return String.format("Name: %s, %d\nAddress: %s", name, age, address);
	}

	public void print() {
		System.out.println(this);
	}

	public static void main(String[] args) {
		SomeDataClass sdc = new SomeDataClass("John Doe", 25, "Baker Street");
		final String ser_path = "/tmp/somedataclass.ser";

		System.out.println("Serializing object...");
		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(ser_path))) {
			out.writeObject(sdc);
		} catch (IOException e) {
			e.printStackTrace();
		}


		SomeDataClass new_sdc = null;
		System.out.println("Deserializing object...");
		try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(ser_path))) {
			new_sdc = (SomeDataClass)in.readObject();	
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.err.println(SomeDataClass.class.getName() + " not found. That's stupid...");
		}

		new_sdc.print();
	}
}
