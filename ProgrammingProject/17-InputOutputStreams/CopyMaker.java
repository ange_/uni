import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;

public class CopyMaker {
	public static boolean copy(String source, String dest) {
		try (
			BufferedReader reader = new BufferedReader(new FileReader(source));
			PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(dest)))
		) {
			String line = null;
			while ((line = reader.readLine()) != null)
				writer.write(line + System.lineSeparator());
		} catch (IOException e) {
			return false;
		}

		return true;
	}

	public static void main(String[] args) {
		if (args.length == 2)
			copy(args[0], args[1]);
		else
			System.err.printf("Usage: java %s [source] [dest]\n", CopyMaker.class.getName());
	}
}
