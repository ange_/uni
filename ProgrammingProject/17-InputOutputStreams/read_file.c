#include <stdio.h>

int main(void) {
	FILE *f;
	if ((f = fopen("./file.txt", "r"))) {
		int num;
		fscanf(f, "%d\n", &num);
		printf("Fetched value: %d\n", num);
		fclose(f);
		return 0;
	}

	return !0;
}

