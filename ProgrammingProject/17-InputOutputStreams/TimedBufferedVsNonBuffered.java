import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;

public class TimedBufferedVsNonBuffered {
	public static void main(String[] args) throws IOException {
		int iter = 10000;
		long startTime = System.currentTimeMillis();

		DataOutputStream dos = new DataOutputStream(new FileOutputStream("nums.bin"));
		for (int i = 0; i < iter; i++) {
			dos.writeByte(2);
			 dos.writeShort(3);
			 dos.writeInt(5);
			 dos.writeLong(23);
			 dos.writeFloat(23.4f);
			 dos.writeDouble(3.21);

		}

		dos.close();

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println(elapsedTime);

		startTime = System.currentTimeMillis();

		dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("nums.bin")));

		for (int i = 0; i < iter; i++) {
			dos.writeByte(2);
			dos.writeShort(3);
			dos.writeInt(5);
			dos.writeLong(23);
			dos.writeFloat(23.4f);
			dos.writeDouble(3.21);
		}

		dos.close();

		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		System.out.println(elapsedTime);
	}
}
