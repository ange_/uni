#include <stdio.h>

int main(void) {
	FILE *f;
	if (! (f = fopen("./file.txt", "w")))
		return !0;

	const int num = 14;
	fprintf(f, "%d", num);
	fclose(f);
	return 0;
}
