import java.io.*;

public class write_test {
	public static void writeBuffered(int iter) throws IOException {
		FileWriter fw2 = new FileWriter("big_file_2");
		BufferedWriter bf = new BufferedWriter(fw2);
		for (int i=0; i<10000; i++)
			bf.write('b');
		bf.close();
	}

	public static void writeBufferedIm(int iter) throws IOException {
		FileWriter fw2 = new FileWriter("big_file_2");
		BufferedWriter bf = new BufferedWriter(fw2);
		for (int i=0; i<10000; i++)
			bf.write('b');
		bf.close();
	}

	public static void main(String[] args) throws IOException {
		long startTime = System.currentTimeMillis();
		writeBuffered(10000);
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println(elapsedTime);

		startTime = System.currentTimeMillis();
		writeBufferedIm(10000);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		System.out.println(elapsedTime);
	}
}
