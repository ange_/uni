import java.io.Serializable;

class Employee implements Serializable {
	public String name;
	public String address;
	public transient int SSN;
	public int number;

	public void mailCheck() {
		System.out.println("Mailing a check to " + name + " " + address);
	}

	/*public void print() {
		System.out.printf("Name: %s\nAddress: %s\nSSN: %d\nNumber: %d\n", name, address, SSN, number);
	}*/
}
