import java.io.*;

public class ByteCounter {
	public static void main(String[] args) throws IOException {
		try (DataOutputStream writer = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("byte_counter.dat")))) {
			writer.writeInt(0);
			System.out.println(writer.size() + " bytes have been written");

			writer.writeDouble(12.45);
			System.out.println(writer.size() + " bytes have been written");
		}
	}
}
