import java.io.FileOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class WriteInts {
	public static void main(String[] args) {
		try (DataOutputStream writer = new DataOutputStream(new FileOutputStream("file.dat"))) {
			writer.writeInt(0);
			writer.writeInt(1);
			writer.writeInt(255);
			writer.writeInt(-1);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
