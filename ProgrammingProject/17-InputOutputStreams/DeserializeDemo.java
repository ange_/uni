import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializeDemo {
	public static void main(String[] args) {
		Employee e = null;
		final String ser_path = "/tmp/employee.ser";

		try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(ser_path))) {
			e = (Employee)in.readObject();
		} catch (IOException i) {
			i.printStackTrace();
		} catch (ClassNotFoundException i) {
			System.err.println("Employee class not found!");
			System.exit(1);
		}

		System.out.println("Deserialized employee...");
		//e.print();
		System.out.printf("Name: %s\nAddress: %s\nSSN: %d\nNumber: %d\n", e.name, e.address, e.SSN, e.number);
	}
}
	
