import java.io.FileInputStream;
import java.io.DataInputStream;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.EOFException;

public class ReadIntsTilEOF {
	public static void main(String[] args) {
		int sum = 0;
		try (DataInputStream reader = new DataInputStream(new BufferedInputStream(new FileInputStream("file.dat")))) {
			for (;;)
				sum += reader.readInt();
		} catch (EOFException e) {
			System.out.println("Result: " + sum);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
