import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;

public class CopyMakerOverEngineered {
	String source, dest;
	BufferedReader reader;
	PrintWriter writer;

	public CopyMakerOverEngineered(String source, String dest) {
		this.source = source;
		this.dest = dest;
	}

	private boolean openFiles() {
		try {
			reader = new BufferedReader(new FileReader(source));
			writer = new PrintWriter(new BufferedWriter(new FileWriter(dest)));
		} catch (IOException e) {
			System.err.println(e.getMessage());
			return false;
		}

		return true;
	}

	private void copyFiles() {
		try {
			String line = null;
			while ((line = reader.readLine()) != null)
				writer.write(line + System.lineSeparator());	
		} catch (IOException e) {
			System.err.println("Error during copy");
		}	
	}

	private void closeFiles() {
		try {
			reader.close();
		} catch (IOException e) {
			System.err.println("Error during " + dest + " close");
		}

		writer.close();
	}

	public static void main(String[] args) {
		if (args.length == 2) {
			CopyMakerOverEngineered cp = new CopyMakerOverEngineered(args[0], args[1]);
			if (cp.openFiles()) {
				cp.copyFiles();
				cp.closeFiles();
			}
		}
		else
			System.err.printf("Usage: java %s [source] [dest]\n", CopyMakerOverEngineered.class.getName());
	}
}
