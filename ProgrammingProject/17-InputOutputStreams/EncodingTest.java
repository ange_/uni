import java.io.UnsupportedEncodingException;

public class EncodingTest {
	public static void main(String[] args) {
		String s = "però";

		try {
			byte[] encoded = s.getBytes("UTF-8");
			System.out.println("Word " + s + " with encoding UTF-8");

			for (int i = 0; i < encoded.length; i++)
				System.out.printf("%X ", encoded[i]);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
}
