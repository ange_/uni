import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializeDemo {
	public static void main(String[] args) {
		Employee e = new Employee();
		e.name = "John Doe";
		e.address = "2nd Street";
		e.SSN = 564738291;
		e.number = 42;

		final String ser_path = "/tmp/employee.ser";
		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(ser_path))) {
			out.writeObject(e);
			System.out.println("Serialized data saved in: " + ser_path);
		} catch (IOException i) {
			i.printStackTrace();
		}
	}
}
