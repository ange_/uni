import java.io.FileOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class WriteString {
	public static void main(String[] args) {
		try (DataOutputStream writer = new DataOutputStream(new FileOutputStream("string_utf-8.dat"))) {
			writer.write("però".getBytes("UTF-16"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
