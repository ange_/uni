import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;
import java.util.Set;
import java.util.ArrayList;

public class AllSets {
	public static void main(String[] args) {
		ArrayList<Set> sets = new ArrayList<>();
		
		sets.add(new TreeSet<String>());
		sets.add(new HashSet<String>());
		sets.add(new LinkedHashSet<String>());

		for (var set : sets) {
			set.add("tallest");
			set.add("shortest");
			set.add("mid");
		}

		
		for (var set : sets) {
			System.out.println(set.getClass().getName());
			System.out.println(set + "\n");
		}
	}
}
