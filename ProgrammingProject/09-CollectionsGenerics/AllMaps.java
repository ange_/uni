import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Map;
import java.util.ArrayList;

public class AllMaps {
	public static void main(String[] args) {
		// var hm = new HashMap<String, Integer>();
		// var lhm = new LinkedHashMap<String, Integer>();
		// var tm = new TreeMap<String, Integer>();
		
		ArrayList<Map> maps = new ArrayList<>();
		
		maps.add(new HashMap<String, Integer>());
		maps.add(new LinkedHashMap<String, Integer>());
		maps.add(new TreeMap<String, Integer>());

		for (var map : maps) {
			map.put("tallest", 150);
			map.put("shortest", 50);
			map.put("mid", 100);
		}

		
		for (var map : maps) {
			System.out.println(map.getClass().getName());
			for (var element : map.entrySet())
				System.out.println(element);

			System.out.println();
		}
	}
}
