public class MyHash {
	public int getHash(String s) {
		byte[] bs = s.getBytes();
		int sum = 0;

		for (int i = 0; i < bs.length; ++i)
			sum += bs[i];
		
		return sum % 100;
	}

	public static void main(String[] args) {
		var test = new MyHash();

		System.out.println(test.getHash("Hello") + "\t" + test.getHash("World"));
		System.out.println(test.getHash("Hello") + "\t" + test.getHash("Hello"));
	}
}
