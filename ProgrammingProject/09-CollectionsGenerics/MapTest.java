import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;

public class MapTest {
	public static void main(String[] args) {
		HashMap<String, String> format = new HashMap<>();
		format.put("font_face", "Times New Roman");
		format.put("font_size", "10");
		format.put("margin", "4");
		format.put("auto_indent", "true");

		//Iterator<HashMap<String, String>> itr = format.entrySet();
		/* Using type inference */
		for (var element : format.entrySet())
			if (element.getKey() != "font_face")
				System.out.println(element.getKey() + ": " + element.getValue());

		for (Map.Entry element : format.entrySet())
			if (element.getKey() != "font_face")
				System.out.println(element.getKey() + ": " + element.getValue());


	}
}
