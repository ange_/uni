import java.util.LinkedList;

public class MyStack {
	private LinkedList<Object> _list = new LinkedList<Object>();

	public void push(Object o) {
		_list.addFirst(o);
	}

	public boolean peek() {
		return ! _list.isEmpty();
	}

	public Object pull() {
		Object o = _list.getFirst();
		_list.removeFirst();
		return o;
	}

	public static void main(String[] args) {
		MyStack s = new MyStack();

		s.push("One");
		s.push("Two");
		s.push("Three");
		
		while (s.peek())
			System.out.println(s.pull());
	}
}
