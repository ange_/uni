import java.util.LinkedList;

public class MyGenericStack<T> {
	private LinkedList<T> _list = new LinkedList<T>();

	public void push(T o) {
		_list.addFirst(o);
	}

	public boolean peek() {
		return ! _list.isEmpty();
	}

	public Object pull() {
		T o = _list.getFirst();
		_list.removeFirst();
		return o;
	}

	public static void main(String[] args) {
		MyGenericStack<String> s = new MyGenericStack<>();

		s.push("One");
		s.push("Two");
		s.push("Three");
		
		while (s.peek())
			System.out.println(s.pull());
	}
}
